package it.polimi.spf.lib.search;


import it.polimi.spf.shared.model.SPFSearchDescriptor;

/**
 * Interface for components which provide low-level access to search functionalities.
 *
 * @author darioarchetti
 * @hide
 */
public interface SearchInterface {

    /**
     * Starts a search
     *
     * @param searchDescriptor - the descriptor of the search
     * @param callback         - the callback to notify the caller that the search has started.
     */
    public void startSearch(SPFSearchDescriptor searchDescriptor, SearchStartedCallback callback);

    /**
     * Stops a search previously registerd
     *
     * @param queryId - the id of the query
     */
    public void stopSearch(String queryId);

    /**
     * Look for a SPF remote instance. Returns true if the istance is reachable (i.e. it is in
     * proximity), otherwise false.
     *
     * @param the identifier of the instance to look for
     *
     * @return true if the instance is reachable
     */
    public boolean lookup(String identifier);

    /**
     * Interface to notify the caller of {@link SearchInterface#startSearch(it.polimi.spf.framework.local.SPFSearchDescriptor,
     * SearchStartedCallback)} that the search has started and to provide it the query id.
     *
     * @author darioarchetti
     * @hide
     */
    public interface SearchStartedCallback {
        /**
         * Notifies tht the query has started.
         *
         * @param queryId - the id of the started query.
         */
        public void run(String queryId);
    }
}
