package it.polimi.spf.lib;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import it.polimi.spf.shared.model.SPFError;

/**
 * Base class for local components implementing the loading pattern. A local component is a wrapper
 * for an AIDL implementing local components, and provides high level functionalities to
 * applications leveraging on SPF. Local components are loaded using an asynchronous pattern where a
 * callback is called when the binding to the remote service is completed.
 *
 * @param <E> - the AIDL interface handled by the local component.
 *
 * @author darioarchetti
 */
public abstract class LocalComponent<E> {

    /**
     * Loads a local component asynchronously.
     *
     * @param context    - the context to use to bind to the service
     * @param descriptor - a {@link Descriptor} to handle the creation of the component;
     * @param callback   - the callback containing the method to call.
     */
    public static <E> void load(final Context context, final Descriptor<E> descriptor, final BaseCallback<E> callback) {
        Utils.notNull(context, "context must not be null");
        Utils.notNull(context, "context must not be null");

        if (AccessTokenManager.get(context).hasToken()) {
            doConnect(context, descriptor, callback);
        } else {
            AccessTokenManager.get(context).requireAccessToken(context, new AccessTokenManager.RegistrationCallback() {

                @Override
                public void onRegistrationSuccessful() {
                    doConnect(context, descriptor, callback);
                }

                @Override
                public void onRegistrationError(SPFError errorMsg) {
                    callback.onError(errorMsg);
                }
            });
        }

    }

    private static <E> void doConnect(final Context context, final Descriptor<E> descriptor, final BaseCallback<E> callback) {
        Intent i = new Intent(descriptor.getIntentName());

        ServiceConnection connection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                E service = descriptor.castInterface(binder);
                LocalComponent<E> c = descriptor.createInstance(context, service, this, callback);
                callback.onServiceReady(c);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                callback.onDisconnect();
            }
        };

        if (context.bindService(i, connection, Context.BIND_AUTO_CREATE) == false) {
            callback.onError(new SPFError(SPFError.SPF_NOT_INSTALLED_ERROR_CODE));
        }
    }

    private E mServiceInterface;
    private Context mContext;
    private ServiceConnection mConnection;
    private BaseCallback<E> mCallback;

    protected LocalComponent(Context context, E serviceInterface, ServiceConnection connection, final BaseCallback<E> callback) {
        this.mServiceInterface = serviceInterface;
        this.mContext = context;
        this.mConnection = connection;
        this.mCallback = new BaseCallback<E>() {

            @Override
            public void onServiceReady(LocalComponent<E> serviceInterface) {
                callback.onServiceReady(serviceInterface);
            }

            @Override
            public void onError(SPFError errorMsg) {
                callback.onError(errorMsg);
            }

            @Override
            public void onDisconnect() {
                callback.onDisconnect();
            }
        };
    }

    public void disconnect() {
        mContext.unbindService(mConnection);
    }

    protected BaseCallback<E> getCallback() {
        return mCallback;
    }

    protected E getService() {
        return mServiceInterface;
    }

    protected Context getContext() {
        return mContext;
    }


    /**
     * Performs common error handling operations. Subclasses may override it to provide specific
     * behavior.
     *
     * @param err
     */
    protected void handleError(SPFError err) {
        if (err.codeEquals(SPFError.TOKEN_NOT_VALID_ERROR_CODE)) {
            AccessTokenManager.get(mContext).invalidateToken();
        }
        mCallback.onError(err);
    }

    protected String getAccessToken() {
        return AccessTokenManager.get(getContext()).getAccessToken();
    }

    public interface BaseCallback<E> {
        public void onServiceReady(LocalComponent<E> serviceInterface);

        public void onError(SPFError err);

        public void onDisconnect();
    }

    /**
     * Contains utility method to handle the binding and the creation of a local component.
     *
     * @param <E> the type of the remote interface
     *
     * @author darioarchetti
     */
    public interface Descriptor<E> {

        /**
         * @return the intent name to be used when binding to the remote service.
         */
        public String getIntentName();

        /**
         * Casts a binder to the interface type handled by the local component.
         *
         * @param binder
         *
         * @return
         */
        public E castInterface(IBinder binder);

        public LocalComponent<E> createInstance(Context context, E serviceInterface, ServiceConnection connection, BaseCallback<E> callback);
    }

}
