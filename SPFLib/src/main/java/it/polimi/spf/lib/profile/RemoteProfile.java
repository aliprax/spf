package it.polimi.spf.lib.profile;

import it.polimi.spf.lib.SPFPerson;
import it.polimi.spf.shared.model.ProfileField;
import it.polimi.spf.shared.model.ProfileFieldContainer;
import it.polimi.spf.shared.model.SPFError;

/**
 * Reference to the profile of a remote person that allows read operations to obtain information.
 *
 * @author darioarchetti
 */
public class RemoteProfile {

    private RemoteProfileInterface mInterface;
    private String mPersonIndentifier;

    /* package */
    public RemoteProfile(SPFPerson p, RemoteProfileInterface iface) {
        this.mInterface = iface;
        this.mPersonIndentifier = p.getIdentifier();
    }


    /**
     * Obtains a bulk of profile fields from a remote profile. The bulk is retrieved as an instance
     * of {@link ProfileFieldContainer} from which the single field can be retrieved.
     *
     * @param fields - the list of {@link ProfileField} to retrieve from the remote profile
     *
     * @return a container that holds the values of requested fields
     */
    public ProfileFieldContainer getProfileBulk(ProfileField<?>... fields) {
        String[] fieldIdentifiers = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            fieldIdentifiers[i] = fields[i].getIdentifier();
        }
        SPFError err = new SPFError();
        return mInterface.getProfileBulk(mPersonIndentifier, fieldIdentifiers, err);
    }
}
