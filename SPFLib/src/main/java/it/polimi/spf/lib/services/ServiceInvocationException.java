package it.polimi.spf.lib.services;

/**
 * Indicates that an error has happened during the execution of the service on the remote device.
 * The error message describes the occurred error.
 */
public class ServiceInvocationException extends Exception {

    private static final long serialVersionUID = 3680399061467176569L;

    public ServiceInvocationException() {
        super();
    }

    public ServiceInvocationException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ServiceInvocationException(String detailMessage) {
        super(detailMessage);
    }

    public ServiceInvocationException(Throwable throwable) {
        super(throwable);
    }

}
