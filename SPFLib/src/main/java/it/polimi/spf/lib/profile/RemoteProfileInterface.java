package it.polimi.spf.lib.profile;


import it.polimi.spf.shared.model.ProfileFieldContainer;
import it.polimi.spf.shared.model.SPFError;

/**
 * Interface for a component that allows low level access to remote profiles.
 *
 * @author darioarchetti
 */
public interface RemoteProfileInterface {


    /**
     * Obtains a bulk of profile fields from a remote profile. The bulk is retrieved as an instance
     * of {@link ProfileFieldContainer} from which the single field can be retrieved.
     *
     * @param identifier - the identifier of the person from which to retrieve the fields
     * @param fields     - the list of {@link ProfileField} to retrieve from the remote profile
     *
     * @return a container that holds the values of requested fields
     */
    public ProfileFieldContainer getProfileBulk(String identifier, String[] fields, SPFError err);
}
