package it.polimi.spf.lib.services.registration;

import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import it.polimi.spf.lib.AccessTokenManager;
import it.polimi.spf.lib.LocalComponent;
import it.polimi.spf.lib.Utils;
import it.polimi.spf.lib.services.ServiceInterface;
import it.polimi.spf.lib.services.ServiceValidator;
import it.polimi.spf.shared.aidl.LocalServiceManager;
import it.polimi.spf.shared.model.SPFError;
import it.polimi.spf.shared.model.SPFServiceDescriptor;

/**
 * Allows applications to register and un-registratier services, and responds to execution requests
 * coming from the SPF framework.
 *
 * @author darioarchetti
 */
public final class LocalServiceRegistry extends LocalComponent<LocalServiceManager> {

    private static final String SERVICE_INTENT = "it.polimi.spf.services.LocalServiceExecutor";
    private static final LocalComponent.Descriptor<LocalServiceManager> DESCRIPTOR = new Descriptor<LocalServiceManager>() {

        @Override
        public String getIntentName() {
            return SERVICE_INTENT;
        }

        @Override
        public LocalServiceManager castInterface(IBinder binder) {
            return LocalServiceManager.Stub.asInterface(binder);
        }

        @Override
        public LocalComponent<LocalServiceManager> createInstance(Context context, LocalServiceManager serviceInterface, ServiceConnection connection, LocalComponent.BaseCallback<LocalServiceManager> callback) {
            return new LocalServiceRegistry(context, serviceInterface, connection, callback);
        }
    };

    /**
     * Loads an instance of {@link LocalServiceRegistry} asynchronously. The callbacks to be called
     * once the connection is ready should be contained into an instance of {@link
     * it.polimi.spf.lib.profile.SPFLocalProfile.Callback}.
     *
     * @param context  - the {@link Context} used to bind to SPF.
     * @param callback - the callback code to be executed once the connection is ready.
     */
    public static void load(final Context context, final Callback callback) {
        LocalComponent.load(context, DESCRIPTOR, asBase(callback));
    }

    protected LocalServiceRegistry(Context context, LocalServiceManager serviceInterface, ServiceConnection connection, BaseCallback<LocalServiceManager> callback) {
        super(context, serviceInterface, connection, callback);
    }

    /**
     * Allows to register a service in the service index. Such service is made available to remote
     * apps.
     *
     * @param serviceInterface - the interface of the service
     */
    public <T> void registerService(Class<? super T> serviceInterface) {
        Utils.notNull(serviceInterface, "serviceInterface must not be null");

        ServiceValidator.validate(serviceInterface, ServiceValidator.TYPE_PUBLISHED);
        ServiceInterface annotation = serviceInterface.getAnnotation(ServiceInterface.class);
        SPFServiceDescriptor descriptor = ServiceInterface.Convert.toServiceDescriptor(annotation);
        String token = AccessTokenManager.get(getContext()).getAccessToken();

        try {
            SPFError error = new SPFError();
            getService().registerService(token, descriptor, error);
            if (!error.isOk()) {
                handleError(error);
            }
        } catch (RemoteException e) {
            catchRemoteException(e);
        }
    }

    /**
     * Allows to unregister a previously registered service.
     *
     * @param serviceInterface
     */
    public <T> void unregisterService(Class<? super T> serviceInterface) {
        Utils.notNull(serviceInterface, "serviceInterface must not be null");
        ServiceValidator.validate(serviceInterface, ServiceValidator.TYPE_PUBLISHED);
        ServiceInterface svcInterface = serviceInterface.getAnnotation(ServiceInterface.class);
        SPFServiceDescriptor svcDesc = ServiceInterface.Convert.toServiceDescriptor(svcInterface);
        String token = AccessTokenManager.get(getContext()).getAccessToken();

        try {
            SPFError error = new SPFError();
            getService().unregisterService(token, svcDesc, error);
            if (!error.isOk()) {
                handleError(error);
            }
        } catch (RemoteException e) {
            catchRemoteException(e);
        }
    }

    public static interface Callback {
        public void onServiceReady(LocalServiceRegistry registry);

        public void onError(SPFError errorMsg);

        public void onDisconnect();
    }

    private static BaseCallback<LocalServiceManager> asBase(final Callback callback) {
        return new BaseCallback<LocalServiceManager>() {

            @Override
            public void onServiceReady(LocalComponent<LocalServiceManager> serviceInterface) {
                callback.onServiceReady((LocalServiceRegistry) serviceInterface);
            }

            @Override
            public void onError(SPFError errorMsg) {
                callback.onError(errorMsg);
            }

            @Override
            public void onDisconnect() {
                callback.onDisconnect();
            }
        };
    }

    private void catchRemoteException(RemoteException e) {
        disconnect();
        getCallback().onDisconnect();
    }
}