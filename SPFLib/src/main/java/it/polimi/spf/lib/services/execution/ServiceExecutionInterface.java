package it.polimi.spf.lib.services.execution;


import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.SPFActivity;

/**
 * Interface for components that provide low level functionalities for executing remote service.
 *
 * @author darioarchetti
 */
public interface ServiceExecutionInterface {

    /**
     * Dispatches an {@link InvocationRequest} to a given target and provides back the outcome,
     * either a result or an exception, of the remote execution wrapped in an {@link
     * InvocationResponse}.
     *
     * @param target  - the identifier of the target to which dispatch the invocation request.
     * @param request - the invocation request
     *
     * @return the response containing the outcome of the invocation.
     */
    public InvocationResponse executeService(String target, InvocationRequest request);

    /**
     * Dispatches an activity to the SPF instances matching the given identifier. The framework will
     * perform information injection into the activity: such information will be available to the
     * caller once the method ends.
     *
     * @param target   - the target of the activity.
     * @param activity - the activity to dispatch.
     *
     * @return - true if the activity has been correctly consumed by the recipient.
     */
    public boolean sendActivity(String target, SPFActivity activity);
}
