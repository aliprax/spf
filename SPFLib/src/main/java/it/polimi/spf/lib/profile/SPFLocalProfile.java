package it.polimi.spf.lib.profile;

import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import it.polimi.spf.lib.LocalComponent;
import it.polimi.spf.shared.aidl.LocalProfileService;
import it.polimi.spf.shared.model.ProfileField;
import it.polimi.spf.shared.model.ProfileFieldContainer;
import it.polimi.spf.shared.model.SPFError;

/**
 * Local component that provides access to the local user profile, allowing read and write
 * operations.
 *
 * @author darioarchetti
 */
public class SPFLocalProfile extends LocalComponent<LocalProfileService> {

    private static final String INTENT_NAME = "it.polimi.spf.framework.appservice.SPFProfileService";
    private static final String TAG = "SPFLocalProfile";
    private static final LocalComponent.Descriptor<LocalProfileService> DESCRIPTOR = new LocalComponent.Descriptor<LocalProfileService>() {

        @Override
        public String getIntentName() {
            return INTENT_NAME;
        }

        @Override
        public LocalProfileService castInterface(IBinder binder) {
            return LocalProfileService.Stub.asInterface(binder);
        }

        @Override
        public LocalComponent<LocalProfileService> createInstance(Context context, LocalProfileService serviceInterface, ServiceConnection connection, LocalComponent.BaseCallback<LocalProfileService> callback) {
            return new SPFLocalProfile(context, serviceInterface, connection, callback);
        }

    };

    /**
     * Loads the local profile connection asynchronously. The operations to perform once the
     * connection is ready should be placed into an implementation of the {@link
     * SPFLocalProfile.Callback} interface.
     *
     * @param context  - the Android {@link Context} to use.
     * @param callback - the callback to execute when the connection is loaded or an error occurs.
     */
    public static void load(final Context context, final Callback callback) {
        LocalComponent.load(context, DESCRIPTOR, asBase(callback));
    }

    private SPFLocalProfile(Context context, LocalProfileService serviceInterface, ServiceConnection connection, BaseCallback<LocalProfileService> callback) {
        super(context, serviceInterface, connection, callback);
    }


    public ProfileFieldContainer getValueBulk(ProfileField<?>... fields) {
        if (fields == null) {
            throw new NullPointerException();
        }

        SPFError err = new SPFError();
        try {
            ProfileFieldContainer pfc = getService().getValueBulk(getAccessToken(), ProfileField.toIdentifierList(fields), err);
            if (err.isOk()) {
                return pfc;
            }
        } catch (RemoteException e) {
            onRemoteException(e);
            err.setCode(SPFError.REMOTE_EXC_ERROR_CODE);
        }
        handleError(err);
        Log.e(TAG, "Remote exception @ setValueBulk");
        return null;
    }

    public boolean setValueBulk(ProfileFieldContainer container) {
        if (container == null) {
            throw new NullPointerException();
        }
        SPFError err = new SPFError();
        try {
            getService().setValueBulk(getAccessToken(), container, err);
            if (err.isOk()) {
                return true;
            }
        } catch (RemoteException e) {
            onRemoteException(e);
            err.setCode(SPFError.REMOTE_EXC_ERROR_CODE);
        }
        handleError(err);
        return false;
    }

    private void onRemoteException(RemoteException e) {
        Log.e(TAG, "Remote exception @ setValue", e);
        getCallback().onError(new SPFError(SPFError.REMOTE_EXC_ERROR_CODE));
    }

    /**
     * Interface of the callback to execute when the connection to the profile is ready, or an error
     * has occurred.
     *
     * @author darioarchetti
     */
    public interface Callback {
        public void onServiceReady(SPFLocalProfile service);

        public void onError(SPFError errorMsg);

        public void onDisconnect();
    }

    /*
     * Convert the custom Callback to a BaseCallback: the one utilized by the
     * LocalComponent class.
     *
     * @param callback - the custom callback
     *
     * @return a BaseCallback
     */
    private static BaseCallback<LocalProfileService> asBase(final Callback callback) {
        return new BaseCallback<LocalProfileService>() {

            @Override
            public void onServiceReady(LocalComponent<LocalProfileService> serviceInterface) {
                callback.onServiceReady((SPFLocalProfile) serviceInterface);
            }

            @Override
            public void onError(SPFError errorMsg) {
                callback.onError(errorMsg);
            }

            @Override
            public void onDisconnect() {
                callback.onDisconnect();
            }
        };
    }
}