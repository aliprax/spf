package it.polimi.spf.lib.services;

/**
 * Indicates that the validation of a service has failed. Error message contains the cause of the
 * failure.
 */
public class IllegalServiceException extends RuntimeException {

    public IllegalServiceException() {
        super();
    }

    public IllegalServiceException(String detailMessage) {
        super(detailMessage);
    }

    private static final long serialVersionUID = 8115160805529364363L;

}
