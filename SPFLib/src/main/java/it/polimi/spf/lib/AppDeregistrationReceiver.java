package it.polimi.spf.lib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AppDeregistrationReceiver extends BroadcastReceiver {

    private static final Object DEREGISTRATION_INTENT = "it.polimi.spf.framework.AppDeregistered";
    private static final String DEREGISTERED_APP = "appIdentifier";
    private static final String TAG = "AppDreregistrationReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals(DEREGISTRATION_INTENT)) {
            return;
        }

        String identifier = intent.getStringExtra(DEREGISTERED_APP);
        if (identifier.equals(Utils.getAppIdentifier(context))) {
            AccessTokenManager.get(context).invalidateToken();
            Log.d(TAG, "App was unregistered from SPF");
        }
    }
}
