package it.polimi.spf.lib.profile;

import android.content.Context;

import it.polimi.spf.lib.SPFComponent;
import it.polimi.spf.lib.SPFPerson;

/**
 * A {@link SPFComponent} that provides access to profiles of remote people to allow read
 * operation.
 *
 * @author darioarchetti
 */
public class SPFRemoteProfile extends SPFComponent {

    private RemoteProfileInterface mInterface;

    public SPFRemoteProfile(Context context, RemoteProfileInterface iface) {
        super(context);
        this.mInterface = iface;
    }

    /**
     * Provides a reference to a remote profile. With this reference it is possible to perform read
     * operations and retrieve information from remote people.
     *
     * @param p - the {@link it.polimi.spf.lib.SPFPerson} whose profile to load.
     *
     * @return an instance of {@link RemoteProfile} to interact with the remote profile.
     */
    public RemoteProfile getProfileOf(SPFPerson p) {
        if (p == null) {
            throw new NullPointerException();
        }

        return new RemoteProfile(p, mInterface);
    }

    @Override
    protected void recycle() {
        mInterface = null;
    }
}
