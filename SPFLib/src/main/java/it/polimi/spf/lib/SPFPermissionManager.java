package it.polimi.spf.lib;

import it.polimi.spf.shared.model.Permission;

public class SPFPermissionManager {

    private static SPFPermissionManager sSingleton;

    public static synchronized SPFPermissionManager get() {
        if (sSingleton == null) {
            sSingleton = new SPFPermissionManager();
        }

        return sSingleton;
    }

    private int mRequiredPermissions = 0;

    public void requirePermission(Permission... permissions) {
        if (permissions == null) {
            throw new NullPointerException();
        }

        for (Permission p : permissions) {
            if (p == null) {
                throw new NullPointerException();
            }

            mRequiredPermissions = mRequiredPermissions | p.getCode();
        }
    }

    public int getRequiredPermission() {
        return mRequiredPermissions;
    }
}
