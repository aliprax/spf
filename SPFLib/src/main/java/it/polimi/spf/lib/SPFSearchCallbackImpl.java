package it.polimi.spf.lib;

import android.os.RemoteException;

import it.polimi.spf.lib.search.SPFSearch;
import it.polimi.spf.shared.aidl.SPFSearchCallback;
import it.polimi.spf.shared.model.BaseInfo;

/**
 * @author aliprax
 */
// TODO hide
public class SPFSearchCallbackImpl extends SPFSearchCallback.Stub {

    SPFSearch spfSearch;

    // TODO hide
    public SPFSearchCallbackImpl(SPFSearch spfSearch) {
        this.spfSearch = spfSearch;
    }

    @Override
    public void onSearchResultReceived(String queryId, String uniqueIdentifier, BaseInfo baseInfo) throws RemoteException {
        spfSearch.onResultFound(queryId, uniqueIdentifier, baseInfo);
    }

    @Override
    public void onSearchResultLost(String queryId, String uniqueIdentifier) throws RemoteException {
        spfSearch.onResultLost(queryId, uniqueIdentifier);
    }

    @Override
    public void onSearchStop(String queryId) throws RemoteException {
        spfSearch.onStop(queryId);

    }

    @Override
    public void onSearchError(String queryId) throws RemoteException {
        spfSearch.onError(queryId);
    }

    @Override
    public void onSearchStart(String queryId) throws RemoteException {
        spfSearch.onSearchStart(queryId);
    }

}
