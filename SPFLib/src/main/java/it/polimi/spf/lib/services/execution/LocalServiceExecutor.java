package it.polimi.spf.lib.services.execution;

import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import it.polimi.spf.lib.AccessTokenManager;
import it.polimi.spf.lib.LocalComponent;
import it.polimi.spf.lib.SPFPerson;
import it.polimi.spf.lib.services.ServiceInterface;
import it.polimi.spf.lib.services.ServiceInvocationException;
import it.polimi.spf.shared.aidl.LocalServiceManager;
import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.SPFActivity;
import it.polimi.spf.shared.model.SPFError;
import it.polimi.spf.shared.model.SPFServiceDescriptor;

/**
 * SPF component to create invocation stubs to invoke remote methods.
 *
 * @author darioarchetti
 */
public final class LocalServiceExecutor extends LocalComponent<LocalServiceManager> {

    private static final String SERVICE_INTENT = "it.polimi.spf.services.LocalServiceExecutor";
    private static final LocalComponent.Descriptor<LocalServiceManager> DESCRIPTOR = new LocalComponent.Descriptor<LocalServiceManager>() {

        @Override
        public String getIntentName() {
            return SERVICE_INTENT;
        }

        @Override
        public LocalServiceManager castInterface(IBinder binder) {
            return LocalServiceManager.Stub.asInterface(binder);
        }

        @Override
        public LocalComponent<LocalServiceManager> createInstance(Context context, LocalServiceManager serviceInterface, ServiceConnection connection, BaseCallback<LocalServiceManager> callback) {
            return new LocalServiceExecutor(context, serviceInterface, connection, callback);
        }
    };

    public static void load(Context context, final Callback callback) {
        LocalComponent.load(context, DESCRIPTOR, asBase(callback));
    }

    protected LocalServiceExecutor(Context context, LocalServiceManager serviceInterface, ServiceConnection connection, BaseCallback<LocalServiceManager> callback) {
        super(context, serviceInterface, connection, callback);
    }

    private InvocationTarget mLocalInvocationTarget = new InvocationTarget() {

        @Override
        public InvocationResponse executeService(InvocationRequest request) throws ServiceInvocationException {
            String token = AccessTokenManager.get(getContext()).getAccessToken();
            try {
                SPFError error = new SPFError();
                InvocationResponse response = getService().executeLocalService(token, request, error);

                if (error.isOk()) {
                    return response;
                } else {
                    handleError(error);
                    throw new ServiceInvocationException(error.getMessage());
                }
            } catch (RemoteException e) {
                catchRemoteException(e);
                throw new ServiceInvocationException(e.getClass().getSimpleName());
            }

        }
    };

    /**
     * Creates an invocation stub to send service invocation requests to the local person performing
     * method calls. The stub is created from a provided service interface, which must be annotated
     * with {@link ServiceInterface} describing the service. The method returns an object
     * implementing the aforementioned interface that can be used to perform method invocation.
     *
     * @param serviceInterface - the interface of the service.
     * @param classLoader      - the ClassLoader to load classes.
     *
     * @return an invocation stub to perform method calls.
     */
    public <E> E createInvocationStub(Class<E> serviceInterface, ClassLoader classLoader) {
        return InvocationStub.from(serviceInterface, classLoader, mLocalInvocationTarget);
    }

    /**
     * Creates an invocation stub to send service invocation requests to a target {@link
     * it.polimi.spf.lib.SPFPerson} providing the name and the parameter list. The object is created
     * from a {@link it.polimi.spf.framework.local.SPFServiceDescriptor} containing the required
     * details.
     *
     * @param target     - the person who the service invocation requests will be dispatched to.
     * @param descriptor - the {@link it.polimi.spf.framework.local.SPFServiceDescriptor} of the
     *                   service whose methods to invoke.
     *
     * @return a {@link ServiceInvocationStub} to perform invocations of remote services.
     */
    public InvocationStub createInvocationStub(SPFPerson target, SPFServiceDescriptor descriptor) {
        return InvocationStub.from(descriptor, mLocalInvocationTarget);
    }

    /**
     * Dispatches an activity to the local SPF instance. The framework will perform information
     * injection into the activity: such information will be available to the caller once the method
     * ends.
     *
     * @param activity - the activity to dispatch.
     *
     * @return - true if the activity has been correctly consumed.
     */
    public boolean sendActivityLocally(SPFActivity activity) {
        String token = getAccessToken();
        SPFError err = new SPFError();
        InvocationResponse resp;

        try {
            getService().injectInformationIntoActivity(token, activity, err);
            if (!err.isOk()) {
                handleError(err);
                return false;
            }

            resp = getService().sendActivityLocally(token, activity, err);
        } catch (RemoteException e) {
            // TODO Error Management
            return false;
        }

        if (!err.isOk()) {
            handleError(err);
            return false;
        }

        if (!resp.isResult()) {
            return false;
        }

        return (Boolean) resp.getResult();
    }

    // We do not let Callback implement BaseCallback, otherwise apllications
    // would need to cast LocalComponent<> to the actual implementation.
    public interface Callback {
        public void onServiceReady(LocalServiceExecutor service);

        public void onError(SPFError errorMsg);

        public void onDisconnect();
    }

    private static BaseCallback<LocalServiceManager> asBase(final Callback callback) {
        return new BaseCallback<LocalServiceManager>() {

            @Override
            public void onServiceReady(LocalComponent<LocalServiceManager> serviceInterface) {
                callback.onServiceReady((LocalServiceExecutor) serviceInterface);
            }

            @Override
            public void onError(SPFError errorMsg) {
                callback.onError(errorMsg);
            }

            @Override
            public void onDisconnect() {
                callback.onDisconnect();
            }
        };
    }

    private void catchRemoteException(RemoteException e) {
        disconnect();
        getCallback().onDisconnect();
    }
}