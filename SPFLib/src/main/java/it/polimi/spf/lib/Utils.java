package it.polimi.spf.lib;

import android.content.Context;

/**
 * Private utility class.
 *
 * @author darioarchetti
 */
public class Utils {

    private Utils() {

    }

    public static <T> T notNull(T object) {
        if (object == null) {
            throw new NullPointerException();
        }

        return object;
    }

    public static <T> T notNull(T object, String msg) {
        if (object == null) {
            throw new NullPointerException(msg);
        }

        return object;
    }

    public static String getAppIdentifier(Context ctx) {
        return ctx.getApplicationInfo().packageName;
    }
}
