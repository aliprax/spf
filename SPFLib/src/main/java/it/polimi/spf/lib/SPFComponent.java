package it.polimi.spf.lib;

import android.content.Context;

/**
 * Common superclass shared by SPF components.
 *
 * @author darioarchetti
 */
public abstract class SPFComponent {

    private Context mContext;

    protected SPFComponent(Context context) {
        this.mContext = context;
    }

    protected final Context getContext() {
        return mContext;
    }

    protected abstract void recycle();
}
