package it.polimi.spf.lib.services.execution;

import android.os.Looper;
import android.util.Log;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import it.polimi.spf.lib.Utils;
import it.polimi.spf.lib.services.ServiceInterface;
import it.polimi.spf.lib.services.ServiceInvocationException;
import it.polimi.spf.lib.services.ServiceValidator;
import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.SPFServiceDescriptor;

/**
 * Invocation stub that allows the invocation of the remote methods of a target {@link
 * it.polimi.spf.lib.SPFPerson}. The methods can be invoked by name, providing the list of
 * parameters. This is useful when the interface of the remote service is not available.
 */
public class InvocationStub {

    /**
     * Creates an invocation stub to send service invocation requests to a target {@link
     * InvocationTarget} performing method calls. The stub is created from a provided service
     * interface, which must be annotated with {@link ServiceInterface} describing the service. The
     * method returns an object implementing the aforementioned interface that can be used to
     * perform method invocation.
     *
     * @param target           - the target who the service invocation requests will be dispatched
     *                         to.
     * @param serviceInterface - the interface of the service.
     * @param classLoader      - the ClassLoader to load classes.
     *
     * @return an invocation stub to perform method calls.
     */
    public static <E> E from(Class<E> serviceInterface, ClassLoader classLoader, InvocationTarget target) {
        Utils.notNull(target, "target must not be null");
        Utils.notNull(serviceInterface, "descriptor must not be null");

        if (classLoader == null) {
            classLoader = Thread.currentThread().getContextClassLoader();
        }

        // Validate service
        ServiceValidator.validate(serviceInterface, ServiceValidator.TYPE_REMOTE);
        ServiceInterface service = serviceInterface.getAnnotation(ServiceInterface.class);
        SPFServiceDescriptor desc = ServiceInterface.Convert.toServiceDescriptor(service);

        InvocationStub stub = InvocationStub.from(desc, target);
        InvocationHandler h = new InvocationHandlerAdapter(stub);
        Object proxy = Proxy.newProxyInstance(classLoader, new Class[]{serviceInterface}, h);

        return serviceInterface.cast(proxy);
    }

    /**
     * Creates an invocation stub to send service invocation requests to a target {@link
     * InvocationTarget} providing the name and the parameter list. The object is created from a
     * {@link it.polimi.spf.framework.local.SPFServiceDescriptor} containing the required details.
     *
     * @param target     - the target who the service invocation requests will be dispatched to.
     * @param descriptor - the {@link it.polimi.spf.framework.local.SPFServiceDescriptor} of the
     *                   service whose methods to invoke.
     *
     * @return a {@link InvocationStub} to perform invocations of remote services.
     */
    public static InvocationStub from(SPFServiceDescriptor descriptor, InvocationTarget target) {
        Utils.notNull(target, "target must not be null");
        Utils.notNull(descriptor, "descriptor must not be null");

        return new InvocationStub(descriptor, target);
    }

    private static final String WRONG_THREAD_MSG = "Remote call to %s.%s made on the UI thread. This may hang your application.";
    private static final String TAG = "InvocationStub";

    private InvocationTarget mInvocationTarget;
    private SPFServiceDescriptor mSvcDesc;

    public InvocationStub(SPFServiceDescriptor descriptor, InvocationTarget target) {
        this.mSvcDesc = descriptor;
        this.mInvocationTarget = target;
    }

    /**
     * Invokes a remote service providing the name of the method to invoke name and the list of
     * parameters. The invocation is a blocking network request and thus should not be performed on
     * the main thread.
     *
     * @param methodName - the name of the method to invoke.
     * @param args       - the array of the parameters to pass to the method.
     *
     * @return the return value of the method, if any, or null if the method returns void.
     *
     * @throws ServiceInvocationException if an invocation is thrown during the execution of the
     *                                    service.
     */
    public Object invokeMethod(String methodName, Object[] args) throws ServiceInvocationException {
        Utils.notNull(methodName);
        Utils.notNull(args);

        checkCurrentThread(methodName);
        InvocationRequest request = new InvocationRequest(mSvcDesc.getAppIdentifier(), mSvcDesc.getServiceName(), methodName, args);
        InvocationResponse response = mInvocationTarget.executeService(request);

        if (response.isResult()) {
            return response.getResult();
        } else {
            throw new ServiceInvocationException(response.getErrorMessage());
        }
    }

    // Checks if the current thread is the main thread, if so it logs a wrning.
    private void checkCurrentThread(String methodName) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            Log.w(TAG, String.format(WRONG_THREAD_MSG, mSvcDesc.getServiceName(), methodName));
        }
    }

    // Adapter class to use an InvocationStub as InvocationHandler in
    // Proxy.newProxyInstance
    private static class InvocationHandlerAdapter implements InvocationHandler {

        private InvocationStub mInvocationStub;

        public InvocationHandlerAdapter(InvocationStub mInvocationStub) {
            this.mInvocationStub = Utils.notNull(mInvocationStub);
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws ServiceInvocationException {
            return mInvocationStub.invokeMethod(method.getName(), args);
        }
    }
}
