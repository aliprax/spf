/**
 *
 */
package it.polimi.spf.lib.notification;

import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.List;

import it.polimi.spf.lib.LocalComponent;
import it.polimi.spf.shared.aidl.SPFNotificationService;
import it.polimi.spf.shared.model.SPFError;
import it.polimi.spf.shared.model.SPFTrigger;

/**
 * TODO comment
 *
 * @author Jacopo Aliprandi
 */
public class SPFNotification extends LocalComponent<SPFNotificationService> {

    private static final String INTENT_NAME = "it.polimi.spf.framework.SPFNotificationService";

    protected SPFNotification(Context context,
                              SPFNotificationService serviceInterface,
                              ServiceConnection connection,
                              LocalComponent.BaseCallback<SPFNotificationService> callback) {
        super(context, serviceInterface, connection, callback);
    }

    // descriptor
    private static final Descriptor<SPFNotificationService> DESCRIPTOR = new Descriptor<SPFNotificationService>() {

        @Override
        public String getIntentName() {
            return INTENT_NAME;
        }

        @Override
        public SPFNotificationService castInterface(IBinder binder) {
            return SPFNotificationService.Stub.asInterface(binder);
        }

        @Override
        public LocalComponent<SPFNotificationService> createInstance(
                Context context, SPFNotificationService serviceInterface,
                ServiceConnection connection,
                LocalComponent.BaseCallback<SPFNotificationService> callback) {
            return new SPFNotification(context, serviceInterface, connection,
                    callback);
        }
    };

    public static void load(Context context, Callback callback) {
        LocalComponent.load(context, DESCRIPTOR, asBase(callback));
    }

    public interface Callback {
        void onServiceReady(SPFNotification notificationSvc);

        void onDisconnected();

        void onError(SPFError msg);
    }

    /*
     *
     * FIXME callback should run in the main thread after the method
     * execution... ...concurrency problem in the client apps
     */
    private static BaseCallback<SPFNotificationService> asBase(
            final Callback callback) {
        return new BaseCallback<SPFNotificationService>() {

            @Override
            public void onServiceReady(
                    LocalComponent<SPFNotificationService> serviceInterface) {
                callback.onServiceReady((SPFNotification) serviceInterface);
            }

            @Override
            public void onError(SPFError errorMsg) {
                callback.onError(errorMsg);
            }

            @Override
            public void onDisconnect() {
                callback.onDisconnected();
            }
        };

    }

    public List<SPFTrigger> listTrigger() {
        try {
            SPFError err = new SPFError(SPFError.NONE_ERROR_CODE);
            List<SPFTrigger> trgs = getService().listTrigger(getAccessToken(), err);
            if (!err.codeEquals(SPFError.NONE_ERROR_CODE)) {
                handleError(err);
            }
            return trgs;
        } catch (RemoteException e) {
            catchRemoteException(e);
            return new ArrayList<SPFTrigger>(0);
        }
    }

    public SPFTrigger getTrigger(long triggerId) {
        try {
            SPFError err = new SPFError(SPFError.NONE_ERROR_CODE);
            SPFTrigger trg = getService().getTrigger(triggerId, getAccessToken(), err);
            if (!err.codeEquals(SPFError.NONE_ERROR_CODE)) {
                handleError(err);
            }
            return trg;
        } catch (RemoteException e) {
            catchRemoteException(e);
            return null;
        }
    }

    public boolean saveTrigger(SPFTrigger trigger) {
        long newId = trigger.getId();
        try {
            SPFError err = new SPFError(SPFError.NONE_ERROR_CODE);
            newId = getService().saveTrigger(trigger, getAccessToken(), err);
            if (!err.codeEquals(SPFError.NONE_ERROR_CODE)) {
                handleError(err);
            } else if (newId != -1) {
                trigger.setId(newId);
                return true;
            }
        } catch (RemoteException e) {
            catchRemoteException(e);
        }
        return false;
    }

    public boolean deleteTrigger(long triggerId) {
        try {
            SPFError err = new SPFError(SPFError.NONE_ERROR_CODE);
            boolean result = getService().deleteTrigger(triggerId, getAccessToken(), err);
            if (!err.codeEquals(SPFError.NONE_ERROR_CODE)) {
                handleError(err);
            }
            return result;
        } catch (RemoteException e) {
            catchRemoteException(e);
            return false;
        }
    }

    public boolean deleteAllTrigger() {
        try {
            SPFError err = new SPFError(SPFError.NONE_ERROR_CODE);
            boolean res = getService().deleteAllTrigger(getAccessToken(), err);
            if (!err.codeEquals(SPFError.NONE_ERROR_CODE)) {
                handleError(err);
            }
            return res;
        } catch (RemoteException e) {
            catchRemoteException(e);
            return false;
        }
    }

    /**
     *
     */
    private void catchRemoteException(RemoteException e) {
        disconnect();
        getCallback().onDisconnect();
    }

}
