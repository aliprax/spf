package it.polimi.spf.lib;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import it.polimi.spf.lib.profile.RemoteProfileInterface;
import it.polimi.spf.lib.profile.SPFRemoteProfile;
import it.polimi.spf.lib.search.SPFSearch;
import it.polimi.spf.lib.search.SearchInterface;
import it.polimi.spf.lib.services.execution.SPFServiceExecutor;
import it.polimi.spf.lib.services.execution.ServiceExecutionInterface;
import it.polimi.spf.shared.aidl.SPFProximityService;
import it.polimi.spf.shared.aidl.SPFSearchCallback;
import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.ProfileFieldContainer;
import it.polimi.spf.shared.model.SPFActivity;
import it.polimi.spf.shared.model.SPFError;
import it.polimi.spf.shared.model.SPFSearchDescriptor;

/**
 * Class that provides a unified access to components for the interaction of people in proximity.
 * The component is loaded in a static way using the {@link #connect(Context, ConnectionListener)}
 * method.
 *
 * @author darioarchetti
 */
public class SPF implements SearchInterface, RemoteProfileInterface, ServiceExecutionInterface {

    public static final int SEARCH_COMPONENT = 0;
    public static final int SERVICE_EXECUTION_COMPONENT = 1;
    public static final int REMOTE_PROFILE_COMPONENT = 2;
    private final static String SERVER_INTENT = "it.polimi.spf.framework.local.SPFServerService";
    private static final String TAG = "SPF";

    /**
     * Creates a connection to SPF asynchronously.
     *
     * @param context  - the context used to bind to SPF service.
     * @param listener - the listener that is notified when the connection to SPF is ready, when the
     *                 connection is closed or when an error occurs.
     */
    public static void connect(final Context context, final ConnectionListener listener) {
        Utils.notNull(context, "Context must not be null");
        Utils.notNull(listener, "Listener must not be null");

        if (AccessTokenManager.get(context).hasToken()) {
            doConnect(context, listener);
        } else {
            AccessTokenManager.get(context).requireAccessToken(context, new AccessTokenManager.RegistrationCallback() {

                @Override
                public void onRegistrationSuccessful() {
                    doConnect(context, listener);
                }

                @Override
                public void onRegistrationError(SPFError errorMsg) {
                    listener.onError(errorMsg);
                }
            });
        }

    }

    private static void doConnect(final Context context, final ConnectionListener listener) {
        ServiceConnection conn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                listener.onDisconnected();
                // TODO lifecycle management
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                SPFProximityService spfServer = SPFProximityService.Stub.asInterface(service);
                SPF instance = new SPF(context, spfServer, this);
                try {
                    instance.init();
                    listener.onConnected(instance);
                } catch (RemoteException e) {
                    listener.onError(new SPFError(SPFError.REMOTE_EXC_ERROR_CODE));
                }
            }
        };

        Intent intent = new Intent(SERVER_INTENT);
        if (context.bindService(intent, conn, Context.BIND_AUTO_CREATE) == false) {
            listener.onError(new SPFError(SPFError.SPF_NOT_INSTALLED_ERROR_CODE));
            Log.d(TAG, "Binding to SPFService failed");
        }

    }

    private SPFProximityService mService;
    private SPFSearchCallback callback;
    private Context mContext;
    private ServiceConnection mConnection;

    // Components
    private SPFSearch spfSearch;
    private SPFRemoteProfile spfRemoteProfile;
    private SPFServiceExecutor spfServiceExecutor;

    private SPF(Context context, SPFProximityService serverService, ServiceConnection serviceConnection) {
        this.mContext = context;
        this.mService = serverService;
        this.mConnection = serviceConnection;
    }

    protected void init() throws RemoteException {
        spfSearch = new SPFSearch(mContext, this);
        callback = new SPFSearchCallbackImpl(spfSearch);

        SPFError err = new SPFError();
        mService.registerCallback(getAccessToken(), callback, err);
        if (!err.isOk()) {
            handleError(err);
        }
    }

    /**
     * Provides access to the different components of SPF. The components are identified by static
     * constants available in this class, and all share a common superclass, {@link SPFComponent};
     * the instances returned vy this method should be casted to the right class. Available services
     * are: <ul> <li>{@link #SEARCH_COMPONENT}: Searching of people in proximity. Returned instance
     * should be casted to {@link SPFSearch}</li> <li>{@link #SERVICE_EXECUTION_COMPONENT}:
     * Execution of remote methods. Returned instance should be casted to {@link
     * SPFServiceExecutor}</li> <li>{@link #REMOTE_PROFILE_COMPONENT}: Retrieval of information from
     * remote profiles. Returned instances should be casted to {@link SPFRemoteProfile}</li> </ul>
     *
     * @param code
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public synchronized <E extends SPFComponent> E getComponent(int code) {
        switch (code) {
            case REMOTE_PROFILE_COMPONENT:
                if (spfRemoteProfile == null) {
                    spfRemoteProfile = new SPFRemoteProfile(getContext(), this);
                }
                return (E) spfRemoteProfile;

            case SEARCH_COMPONENT:
                if (spfSearch == null) {
                    spfSearch = new SPFSearch(getContext(), this);
                }
                return (E) spfSearch;
            case SERVICE_EXECUTION_COMPONENT:
                if (spfServiceExecutor == null) {
                    spfServiceExecutor = new SPFServiceExecutor(getContext(), this);
                }
                return (E) spfServiceExecutor;
            default:
                throw new IllegalArgumentException("Component code " + code + " not found.");
        }
    }

    /**
     * Disconnects from SPF. All created components will stop working after this method is invoked.
     */
    public void disconnect() {
        try {
            SPFError err = new SPFError();
            mService.unregisterCallback(getAccessToken(), err);
            if (err.isOk()) {
                handleError(err);
            }

            // TODO release resources
            // spfSearch.recycle();
            // spfRemoteProfile.recycle();
            // spfServiceExecutor.recycle();

        } catch (RemoteException e) {
            // no code here
        }
        mContext.unbindService(mConnection);
    }

    /**
     * @return the context used to connect to SPF.
     */
    public Context getContext() {
        return mContext;
    }

    /*
     * Implementation of SearchInterface (non-Javadoc)
     *
     * @see
     * it.polimi.spf.lib.search.SearchInterface#startSearch(it.polimi
     * .spf.framework.local.SPFSearchDescriptor,
     * it.polimi.spf.lib.search.SearchInterface.SearchStartedCallback)
     */
    @Override
    public void startSearch(SPFSearchDescriptor searchDescriptor, SearchStartedCallback callback) {
        try {
            SPFError err = new SPFError();
            String queryId = mService.startNewSearch(getAccessToken(), searchDescriptor, err);
            if (err.isOk()) {
                callback.run(queryId);
            } else {
                handleError(err);
            }
        } catch (RemoteException e) {
            // TODO Error management
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * it.polimi.spf.lib.search.SearchInterface#stopSearch(java.lang
     * .String)
     */
    @Override
    public void stopSearch(String queryId) {
        try {
            SPFError err = new SPFError();
            mService.stopSearch(getAccessToken(), queryId, err);
            if (!err.isOk()) {
                handleError(err);
            }
        } catch (RemoteException e) {
            // TODO: error management
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * it.polimi.spf.lib.search.SearchInterface#lookup(java.lang.String
     * )
     */
    @Override
    public boolean lookup(String identifier) {
        try {
            SPFError err = new SPFError();
            boolean found = mService.lookup(getAccessToken(), identifier, err);
            if (err.isOk()) {
                return found;
            } else {
                handleError(err);
                return false;
            }
        } catch (RemoteException e) {
            // TODO Error Management
            Log.d(TAG, "Remote exception while executing lookup on " + identifier, e);
            return false;
        }

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * it.polimi.spf.lib.profile.RemoteProfileInterface#getProfileBulk
     * (java.lang.String, java.lang.String[],
     * it.polimi.spf.framework.local.SPFError)
     */
    @Override
    public ProfileFieldContainer getProfileBulk(String identifier, String[] fields, SPFError err) {
        String accessToken = AccessTokenManager.get(mContext).getAccessToken();
        try {
            ProfileFieldContainer pfc = mService.getProfileBulk(accessToken, identifier, fields, err);
            if (err.isOk()) {
                return pfc;
            }
        } catch (RemoteException e) {
            Log.e(TAG, "Error @ getProfileBulk", e);
        }
        handleError(err);
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * it.polimi.spf.lib.services.execution.ServiceExecutionInterface
     * #executeService(java.lang.String,
     * it.polimi.spf.framework.local.InvocationRequest,
     * it.polimi.spf.framework.local.SPFError)
     */
    @Override
    public InvocationResponse executeService(String target, InvocationRequest request) {
        try {
            SPFError err = new SPFError();
            String token = AccessTokenManager.get(mContext).getAccessToken();

            for (Object param : request.getParams()) {
                if (param instanceof SPFActivity) {
                    mService.injectInformationIntoActivity(token, target, (SPFActivity) param, err);
                    if (!err.isOk()) {
                        handleError(err);
                        return InvocationResponse.error(err.getMessage());
                    }
                }
            }

            InvocationResponse resp = mService.executeRemoteService(token, target, request, err);
            if (!err.isOk()) {
                handleError(err);
                return InvocationResponse.error(err.getMessage());
            }
            return resp;
        } catch (RemoteException e) {
            return InvocationResponse.error(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * it.polimi.spf.lib.services.execution.ServiceExecutionInterface
     * #sendActivity(it.polimi.spf.framework.local.SPFActivity)
     */
    @Override
    public boolean sendActivity(String target, SPFActivity activity) {
        String token = getAccessToken();
        SPFError err = new SPFError();
        InvocationResponse resp;

        try {
            mService.injectInformationIntoActivity(token, target, activity, err);
            if (!err.isOk()) {
                handleError(err);
                return false;
            }

            resp = mService.sendActivity(token, target, activity, err);
        } catch (RemoteException e) {
            // TODO Error Management
            return false;
        }

        if (!err.isOk()) {
            handleError(err);
            return false;
        }

        if (!resp.isResult()) {
            return false;
        }

        return (Boolean) resp.getResult();
    }

    private void handleError(SPFError err) {
        if (err.codeEquals(SPFError.TOKEN_NOT_VALID_ERROR_CODE)) {
            AccessTokenManager.get(mContext).invalidateToken();
        }

    }

    private String getAccessToken() {
        return AccessTokenManager.get(mContext).getAccessToken();
    }

    /**
     * Interface for components that wants to be notified of the status of the connection with SPF.
     *
     * @author darioarchetti
     */
    public static interface ConnectionListener {

        /**
         * Called when the connection to SPF is available.
         *
         * @param instance - the instance of SPF to use to interact with people in the proximity.
         */
        public void onConnected(SPF instance);

        /**
         * Called when an error occurs.
         */
        public void onError(SPFError errorMsg);

        /**
         * Called when the connection with SPF is closed.
         */
        public void onDisconnected();

    }
}
