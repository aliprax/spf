/**
 *
 */
package it.polimi.spf.lib.search;

import android.content.Context;
import android.util.SparseArray;

import java.util.Hashtable;
import java.util.Map;

import it.polimi.spf.lib.LooperUtils;
import it.polimi.spf.lib.SPFComponent;
import it.polimi.spf.lib.SPFPerson;
import it.polimi.spf.shared.model.BaseInfo;
import it.polimi.spf.shared.model.SPFSearchDescriptor;

/**
 * A {@link SPFComponent} that allows applications to search for remote people who match given
 * criteria.
 */
public final class SPFSearch extends SPFComponent {

    private SparseArray<String> mTagToId;
    private SearchInterface mSearchInterface;
    private Map<String, SearchCallback> mCallbacks;

    // TODO @hide
    public SPFSearch(Context context, SearchInterface searchInterface) {
        super(context);
        mTagToId = new SparseArray<String>();
        mCallbacks = new Hashtable<String, SPFSearch.SearchCallback>();
        mSearchInterface = searchInterface;
    }

    /**
     * Starts a search for remote people. The application must provide a tag, used to replace
     * equivalent queries, a {@link it.polimi.spf.framework.local.SPFSearchDescriptor} containing
     * the configuration of the search, and a {@link SearchCallback} to be notified of found
     * results.
     *
     * @param tag              - the tag identifying this query
     * @param searchDescriptor - the descriptor of the search
     * @param callback         - the callback to be notified of results.
     */
    public void startSearch(final int tag, SPFSearchDescriptor searchDescriptor, final SearchCallback callback) {
        // replace equivalent query ( equivalent == same tag )
        if (mTagToId.get(tag) != null) {
            String queryId = mTagToId.get(tag);
            mTagToId.delete(tag);
            if (queryId != null && mCallbacks.remove(queryId) != null) {
                mSearchInterface.stopSearch(queryId);
            }
        }
        mSearchInterface.startSearch(searchDescriptor, new SearchInterface.SearchStartedCallback() {

            @Override
            public void run(String queryId) {
                if (queryId == null) {
                    callback.onSearchError();
                    return;
                }

                SearchCallback uiCallback = LooperUtils.onMainThread(SearchCallback.class, callback);

                mCallbacks.put(queryId, uiCallback);
                mTagToId.put(tag, queryId);
                uiCallback.onSearchStart();
            }
        });

    }

    /**
     * Stops a previously registered search request performed by the application. The application
     * must provide the tag it registered the search with. The callback associated to the search
     * does not receive any further notification.
     *
     * @param tag - the tag used to register the search.
     */
    public void stopSearch(int tag) {
        String queryId = mTagToId.get(tag);
        mTagToId.delete(tag);
        if (queryId != null && mCallbacks.remove(queryId) != null) {
            mSearchInterface.stopSearch(queryId);
        }
    }

    /**
     * Stops all searches registered by the application.
     *
     * @see SPFSearch#stopSearch(int)
     */
    public void stopAllSearch() {
        mTagToId.clear();
        String[] queryIds = (String[]) mCallbacks.keySet().toArray();
        mCallbacks.clear();
        for (String queryId : queryIds) {
            mSearchInterface.stopSearch(queryId);
        }
    }

    /**
     * Allows to retrieve a reference to a remote person given its identifier. This reference is
     * valid until the given person is reachable from the proximity middleware.
     *
     * @param identifier
     *
     * @return
     */
    public SPFPerson lookupFoundPerson(String identifier) {

        boolean isReachable = mSearchInterface.lookup(identifier);
        if (isReachable) {
            return new SPFPerson(identifier);
        } else {
            return null;
        }

    }

    /**
     * Interface for components that can receive updates on a registered search request.
     *
     * @author darioarchetti
     */
    public interface SearchCallback {
        /**
         * Called when a person matching the criteria of the search is found.
         *
         * @param p - the found person.
         */
        void onPersonFound(SPFPerson p);

        /**
         * Called when the contact with a previously found person is lost.
         *
         * @param p - the person we are no more in contact with.
         */
        void onPersonLost(SPFPerson p);

        /**
         * Called when the search is stopped.
         */
        void onSearchStop();

        /**
         * Called when an error occurs during the search.
         */
        void onSearchError();

        void onSearchStart();
    }

    // TODO hide
    public void onResultLost(String queryId, String uniqueIdentifier) {
        SearchCallback callback = mCallbacks.get(queryId);
        if (callback == null) {
            return;
        }
        callback.onPersonLost(new SPFPerson(uniqueIdentifier));
    }

    // TODO hide
    public void onResultFound(String queryId, String uniqueIdentifier, BaseInfo baseInfo) {
        SearchCallback callback = mCallbacks.get(queryId);
        if (callback == null) {
            mSearchInterface.stopSearch(queryId);
            return;
        }
        SPFPerson p = new SPFPerson(uniqueIdentifier, baseInfo);
        callback.onPersonFound(p);

    }

    // TODO hide
    public void onStop(String queryId) {
        SearchCallback callback = mCallbacks.get(queryId);
        if (callback == null) {
            mSearchInterface.stopSearch(queryId);
            return;
        }

        callback.onSearchStop();
    }

    // TODO hide
    public void onError(String queryId) {
        SearchCallback callback = mCallbacks.get(queryId);
        if (callback == null) {
            mSearchInterface.stopSearch(queryId);
            return;
        }

        callback.onSearchError();
    }

    public void onSearchStart(String queryId) {
        SearchCallback callback = mCallbacks.get(queryId);
        if (callback == null) {
            mSearchInterface.stopSearch(queryId);
            return;
        }

        callback.onSearchStart();
    }

    @Override
    protected void recycle() {
        // TODO Auto-generated method stub

    }

}
