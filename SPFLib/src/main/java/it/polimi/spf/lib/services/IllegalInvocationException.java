package it.polimi.spf.lib.services;

/**
 * Indicates that the invocation of a service has failed due error in the configuration, for example
 * parameters mismatch.
 */
public class IllegalInvocationException extends Exception {

    private static final long serialVersionUID = -4848082620238254886L;

    public IllegalInvocationException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public IllegalInvocationException(Throwable throwable) {
        super(throwable);
    }

    public IllegalInvocationException() {
        super();
    }

    public IllegalInvocationException(String detailMessage) {
        super(detailMessage);
    }

}