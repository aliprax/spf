package it.polimi.spf.lib.services.execution;

import android.content.Context;

import it.polimi.spf.lib.SPFComponent;
import it.polimi.spf.lib.SPFPerson;
import it.polimi.spf.lib.services.ServiceInterface;
import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.SPFActivity;
import it.polimi.spf.shared.model.SPFServiceDescriptor;

/**
 * A {@link SPFComponent} that allows the invocation of the services of a remote person.
 *
 * @author darioarchetti
 */
public class SPFServiceExecutor extends SPFComponent {

    private ServiceExecutionInterface mExecutionInterface;

    // TODO @hide
    public SPFServiceExecutor(Context context, ServiceExecutionInterface executionInterface) {
        super(context);
        this.mExecutionInterface = executionInterface;
    }

    /**
     * Creates an invocation stub to send service invocation requests to a target {@link
     * it.polimi.spf.lib.SPFPerson} performing method calls. The stub is created from a provided
     * service interface, which must be annotated with {@link ServiceInterface} describing the
     * service. The method returns an object implementing the aforementioned interface that can be
     * used to perform method invocation.
     *
     * @param target           - the person who the service invocation requests will be dispatched
     *                         to.
     * @param serviceInterface - the interface of the service.
     * @param classLoader      - the ClassLoader to load classes.
     *
     * @return an invocation stub to perform method calls.
     */
    public <E> E createInvocationStub(final SPFPerson target, Class<E> serviceInterface, ClassLoader classLoader) {
        return InvocationStub.from(serviceInterface, classLoader, new InvocationTarget() {

            @Override
            public InvocationResponse executeService(InvocationRequest request) {
                return mExecutionInterface.executeService(target.getIdentifier(), request);
            }
        });
    }

    /**
     * Creates an invocation stub to send service invocation requests to a target {@link
     * it.polimi.spf.lib.SPFPerson} providing the name and the parameter list. The object is created
     * from a {@link it.polimi.spf.framework.local.SPFServiceDescriptor} containing the required
     * details.
     *
     * @param target     - the person who the service invocation requests will be dispatched to.
     * @param descriptor - the {@link it.polimi.spf.framework.local.SPFServiceDescriptor} of the
     *                   service whose methods to invoke.
     *
     * @return a {@link InvocationStub} to perform invocations of remote services.
     */
    public InvocationStub createInvocationStub(final SPFPerson target, SPFServiceDescriptor descriptor) {
        return InvocationStub.from(descriptor, new InvocationTarget() {

            @Override
            public InvocationResponse executeService(InvocationRequest request) {
                return mExecutionInterface.executeService(target.getIdentifier(), request);
            }
        });
    }

    /**
     * Dispatches an activity to the SPF instances matching the given identifier. The framework will
     * perform information injection into the activity: such information will be available to the
     * caller once the method ends.
     *
     * @param target   - the target of the activity.
     * @param activity - the activity to dispatch.
     *
     * @return - true if the activity has been correctly consumed by the recipient.
     */
    public boolean sendActivity(String target, SPFActivity activity) {
        return mExecutionInterface.sendActivity(target, activity);
    }

    @Override
    protected void recycle() {
        // TODO Auto-generated method stub

    }
}
