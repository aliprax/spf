package it.polimi.spf.lib.services.registration;

import it.polimi.spf.lib.services.IllegalInvocationException;
import it.polimi.spf.lib.services.ServiceInterface;
import it.polimi.spf.lib.services.ServiceValidator;
import it.polimi.spf.lib.services.activities.ActivityConsumer;
import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.SPFServiceDescriptor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Wrapper class for services that eases the invocation of methods. A service is
 * made up of a ServiceInterface (an interface annotated with
 * {@link ServiceInterface}) that describes the service and its methods, and of
 * an implementation of such interface.
 */
public class ServiceWrapper {

	private static class ErrorMsg {
		public final static String METHOD_NOT_FOUND = "Method %s not found in service %s.";
		public final static String ILLEGAL_ARGUMENT = "Illegal argument provided for method invocation.";
	}

	private SPFServiceDescriptor mServiceDescriptor;
	private Object mImplementation;
	private Map<String, Method> mMethodIndex;

	/**
	 * Creates a new wrapper for the given service. The given Service Interface
	 * will be validated (see {@link ServiceValidator} for the constraints),
	 * illegal service interface will cause an unchecked
	 * IllegalServiceException.
	 * 
	 * @param context
	 *            - the Android context
	 * @param serviceInterface
	 *            - the service interface
	 * @param implementation
	 *            - the service implementation
	 */
	public ServiceWrapper(Class<?> serviceInterface, Object implementation) {
		ServiceValidator.validate(serviceInterface, ServiceValidator.TYPE_PUBLISHED);
		ServiceInterface service = serviceInterface.getAnnotation(ServiceInterface.class);

		this.mServiceDescriptor = ServiceInterface.Convert.toServiceDescriptor(service);
		this.mImplementation = implementation;
		this.mMethodIndex = new HashMap<String, Method>();

		for (Method m : serviceInterface.getMethods()) {
			if (m.isAnnotationPresent(ActivityConsumer.class)) {
				continue;
			}
			
			mMethodIndex.put(m.getName(), m);
		}
	}

	/**
	 * Invokes a method of the service.
	 * 
	 * @param request
	 *            - The invocation request
	 * @return the return value
	 * @throws IllegalInvocationException
	 *             if the request does not match the service
	 * @throws InvocationTargetException
	 *             if an Exception is thrown during execution
	 */
	public Object invokeMethod(InvocationRequest request) throws IllegalInvocationException, InvocationTargetException {
		String methodName = request.getMethodName();
		Object[] params = request.getParams();

		if (!mMethodIndex.containsKey(methodName)) {
			String msg = String.format(ErrorMsg.METHOD_NOT_FOUND, methodName, mServiceDescriptor.getServiceName());
			throw new IllegalInvocationException(msg);
		}

		try {
			return mMethodIndex.get(methodName).invoke(mImplementation, params);
		} catch (IllegalAccessException e) {
			// Should never happen as only public methods of interfaces can be
			// invoked.
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new IllegalInvocationException(ErrorMsg.ILLEGAL_ARGUMENT, e);
		}
	}

	/**
	 * @return the name of the available methods.
	 */
	public Set<String> getAvailableMethods() {
		return mMethodIndex.keySet();
	}

	/**
	 * @return the {@link it.polimi.spf.framework.local.SPFServiceDescriptor} of this service.
	 */
	public SPFServiceDescriptor getServiceDescriptor() {
		return mServiceDescriptor;
	}

	public static <T> String readServiceName(Class<? super T> serviceInterface) {
		ServiceInterface service = serviceInterface.getAnnotation(ServiceInterface.class);
		if (service == null) {
			throw new IllegalArgumentException("Service interface does not have a ServiceInterface annotation.");
		}

		return service.name();
	}
}