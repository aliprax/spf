package it.polimi.spf.lib.services;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.polimi.spf.lib.Utils;
import it.polimi.spf.lib.services.execution.SPFServiceExecutor;
import it.polimi.spf.lib.services.registration.LocalServiceRegistry;
import it.polimi.spf.shared.model.SPFServiceDescriptor;

/**
 * Annotation for interfaces that describe services. Services are collection of methods that can be
 * invoked by remote instances of SPF. This annotation must be present on interfaces intended: <ul>
 * <li>to be registered in {@link LocalServiceRegistry}</li> <li>to be invoked using {@link
 * SPFServiceExecutor}</li> </ul>
 *
 * @author darioarchetti
 */

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
public @interface ServiceInterface {

    /**
     * The identifier of the app who registered the service. It is mandatory only for services to be
     * executed. In service registration, this information is ignored.
     */
    public String app() default "";

    /**
     * The name of the service. If not available, it is retrieved from the name of the annotated
     * interface.
     */
    public String name() default "";

    /**
     * The version of the service. It is always mandatory.
     */
    public String version();

    /**
     * The intent that will be used by SPF to bind to the {@link SPFExecutionEndpointService}
     * exposed by the application to dispatch method execution requests. It is mandatory for
     * services that are being registered.
     */
    public String intent() default "";

    /**
     * The list of {@link SPFActivity} verbs supported by the service.
     *
     * @return
     */
    public String[] consumedVerbs() default {};

    /**
     * Utility class to convert the an instance of {@link ServiceInterface} into one of {@link
     * it.polimi.spf.framework.local.SPFServiceDescriptor} containing the same information.
     *
     * @author darioarchetti
     */
    public final static class Convert {
        private Convert() {
        }

        /**
         * Converts an instance of {@link ServiceInterface} into one of {@link
         * it.polimi.spf.framework.local.SPFServiceDescriptor} containing the same interface.
         *
         * @param svcInterface
         *
         * @return
         */
        public final static SPFServiceDescriptor toServiceDescriptor(ServiceInterface svcInterface) {
            Utils.notNull(svcInterface, "svcInterface must not be null");
            return new SPFServiceDescriptor(svcInterface.name(), svcInterface.app(), svcInterface.version(), svcInterface.intent(), svcInterface.consumedVerbs());
        }
    }
}
