package it.polimi.spf.lib;

import it.polimi.spf.lib.profile.RemoteProfile;
import it.polimi.spf.lib.profile.SPFRemoteProfile;
import it.polimi.spf.lib.services.execution.SPFServiceExecutor;
import it.polimi.spf.shared.model.BaseInfo;
import it.polimi.spf.shared.model.SPFActivity;

/**
 * Instances of the {@code SPFPerson} class represent people found in the proximity of the device
 * with whom the interaction is possible.
 */
public class SPFPerson {

    public static final String SELF_IDENTIFIER = "#SELF";
    private String mIdentifier;
    private BaseInfo mBaseInfo;

    // TODO @hide
    public SPFPerson(String uniqueIdentifier) {
        this(uniqueIdentifier, null);
    }

    // TODO @hide
    public SPFPerson(String uniqueIdentifier, BaseInfo baseInfo) {
        this.mIdentifier = uniqueIdentifier != null ? uniqueIdentifier : SELF_IDENTIFIER;
        this.mBaseInfo = baseInfo;
    }

    /**
     * Returns the identifier of the person. If the person represents the user himself, the methods
     * returns the value contained in the constant SELF_IDENTIFIER.
     *
     * @return the identifier of the person
     */
    public String getIdentifier() {
        return mIdentifier;
    }

    /**
     * Returns the basic information of the person. If it is unavailable, an empty string is
     * retrieved.
     *
     * @return the basic information about the person
     */
    public BaseInfo getBaseInfo() {
        return mBaseInfo;
    }

    /**
     * @return true if the person is self
     */
    public boolean isSelf() {
        return mIdentifier.equals(SELF_IDENTIFIER);
    }

    @Override
    public String toString() {
        return getIdentifier();
    }

    /**
     * Shorthand method to retrieve the RemoteProfile of {@code this} person.
     *
     * @param spf
     *
     * @return
     */
    public RemoteProfile getProfile(SPF spf) {
        SPFRemoteProfile rp = (SPFRemoteProfile) spf.getComponent(SPF.REMOTE_PROFILE_COMPONENT);
        return rp.getProfileOf(this);
    }

    public boolean sendActivity(SPF spf, SPFActivity activity) {
        SPFServiceExecutor executor = spf.getComponent(SPF.SERVICE_EXECUTION_COMPONENT);
        return executor.sendActivity(mIdentifier, activity);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof SPFPerson)) {
            return false;
        }

        return ((SPFPerson) o).mIdentifier.equals(mIdentifier);
    }

    @Override
    public int hashCode() {
        return mIdentifier.hashCode();
    }
}