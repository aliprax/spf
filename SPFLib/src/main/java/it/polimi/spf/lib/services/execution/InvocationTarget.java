package it.polimi.spf.lib.services.execution;

import it.polimi.spf.lib.services.ServiceInvocationException;
import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;

/**
 * Interface for components that can receive invocation requests. Used to abstract the target of an
 * invocation that may be the local instance of SPF or a remote one, thus making {@link
 * InvocationStub} able to work in both cases.
 *
 * @author darioarchetti
 */
public interface InvocationTarget {

    /**
     * Low level call to dispatch invocation requests.
     *
     * @param request - the {@link InvocationRequest} to be dispatched.
     *
     * @return an instance of {@link InvocationResponse} containing the result of the invocation.
     *
     * @throws ServiceInvocationException if an instance is thrown during the execution of the
     *                                    service.
     */
    public InvocationResponse executeService(InvocationRequest request) throws ServiceInvocationException;

}
