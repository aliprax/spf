package it.polimi.spf.app.fragments.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import it.polimi.spf.app.R;
import it.polimi.spf.framework.profile.SPFPersona;

public class ProfileEditActivity extends Activity {

    public static final String EXTRA_PERSONA = "persona";

    private ProfileFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        if (savedInstanceState == null) {
            SPFPersona persona = getIntent().getExtras().getParcelable(EXTRA_PERSONA);
            mFragment = ProfileFragment.createEditSelfProfileFragment(persona);
            getFragmentManager().beginTransaction().replace(R.id.container, mFragment).commit();
        } else {
            mFragment = (ProfileFragment) getFragmentManager().findFragmentById(R.id.container);
        }

        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mFragment.onCreateOptionsMenu(menu, getMenuInflater());
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return mFragment.onOptionsItemSelected(item);
        }
    }

    @Override
    public void finish() {
        setResult(mFragment.isContainerModifiedAtLeastOnce() ? RESULT_OK : RESULT_CANCELED);

        if (mFragment.isContainerModified()) {
            new AlertDialog.Builder(this).setMessage(R.string.profileedit_confirm_message).setPositiveButton(R.string.profileedit_confirm_yes, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setResult(RESULT_CANCELED);
                    ProfileEditActivity.super.finish();
                }
            }).setNegativeButton(R.string.profileedit_confirm_no, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        } else {
            super.finish();
        }
    }
}