package it.polimi.spf.app.fragments.contacts;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.polimi.spf.app.R;
import it.polimi.spf.app.view.CirclePicker;
import it.polimi.spf.app.view.PersonCard;
import it.polimi.spf.framework.SPF;
import it.polimi.spf.framework.security.PersonInfo;

public class ContactConfirmDialogView extends LinearLayout {

    private CirclePicker mCirclePicker;
    private PersonCard mPersonCard;
    private EditText mPassphrase;

    public ContactConfirmDialogView(Context context, PersonInfo personInfo) {
        super(context);
        init(context);
        mPersonCard.show(personInfo);
    }

    public ContactConfirmDialogView(Context context, String displayName, Bitmap picture) {
        super(context);
        init(context);
        mPersonCard.setName(displayName);
        if (picture == null) {
            mPersonCard.setPictureFromResource(R.drawable.empty_profile_picture);
        } else {
            mPersonCard.setPicture(new BitmapDrawable(getResources(), picture));
        }
    }

    private void init(Context context) {
        inflate(context, R.layout.contacts_people_confirm_alert, this);
        mPersonCard = (PersonCard) findViewById(R.id.contacts_people_confirm_card);

        mCirclePicker = (CirclePicker) findViewById(R.id.contacts_people_confirm_circles);
        Collection<String> availableCircles = SPF.get().getSecurityMonitor().getPersonRegistry().getCircles();
        mCirclePicker.setCircles(null, new ArrayList<String>(availableCircles));

        mPassphrase = (EditText) findViewById(R.id.contacts_people_confirm_passphrase);
    }

    public List<String> getSelectedCircles() {
        return mCirclePicker.getSelectedCircles();
    }

    public String getPassphrase() {
        return mPassphrase.getText().toString();
    }
}
