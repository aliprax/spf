package it.polimi.spf.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import it.polimi.spf.framework.security.PermissionHelper;
import it.polimi.spf.shared.model.Permission;

public class PermissionArrayAdapter extends ArrayAdapter<Permission> {
    public PermissionArrayAdapter(Context context, Permission[] permissions) {
        super(context, android.R.layout.simple_list_item_1, permissions);

        if (context == null || permissions == null) {
            throw new NullPointerException();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView == null ?
                LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false) :
                convertView;

        Permission p = getItem(position);
        ViewHolder.from(v).textView.setText(PermissionHelper.getPermissionFriendlyName(p, getContext()));
        return v;
    }

    private static class ViewHolder {
        public TextView textView;

        public static ViewHolder from(View v) {
            Object o = v.getTag();
            if (o != null && (o instanceof ViewHolder)) {
                return (ViewHolder) o;
            }

            ViewHolder holder = new ViewHolder();
            v.setTag(holder);
            holder.textView = (TextView) v.findViewById(android.R.id.text1);
            return holder;
        }
    }

}
