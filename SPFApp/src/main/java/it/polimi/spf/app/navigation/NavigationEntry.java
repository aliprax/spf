package it.polimi.spf.app.navigation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.polimi.spf.app.R;

public class NavigationEntry extends LinearLayout {

    private TextView mNameView, mNotificationView;

    public NavigationEntry(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NavigationEntry(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NavigationEntry(Context context) {
        super(context);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.navigation_fragment_entry, this);
        mNameView = (TextView) findViewById(R.id.navigation_entry_name);
        mNotificationView = (TextView) findViewById(R.id.navigation_entry_notification);
    }

    public void setName(String name) {
        mNameView.setText(name);
    }

    public void showNotification(String text) {
        mNotificationView.setVisibility(View.VISIBLE);
        mNotificationView.setText(text);
    }

    public void clearNotification() {
        mNotificationView.setText("");
        mNotificationView.setVisibility(View.GONE);
    }

}
