package it.polimi.spf.app.fragments.contacts;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import it.polimi.spf.app.view.PersonCard;
import it.polimi.spf.framework.security.PersonInfo;

public class FriendListArrayAdapter extends ArrayAdapter<PersonInfo> {

    public FriendListArrayAdapter(Context context) {
        super(context, android.R.layout.simple_expandable_list_item_1);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PersonCard card = convertView == null ? new PersonCard(getContext()) : (PersonCard) convertView;
        card.show(getItem(position));
        return card;
    }
}
