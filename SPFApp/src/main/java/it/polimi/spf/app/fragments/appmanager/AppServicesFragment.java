package it.polimi.spf.app.fragments.appmanager;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import it.polimi.spf.app.R;
import it.polimi.spf.framework.SPF;
import it.polimi.spf.framework.security.AppAuth;

public class AppServicesFragment extends Fragment {

    private AppAuth mAppAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.appmanager_service_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ListView serviceList = (ListView) getView().findViewById(R.id.appmanager_service_list);

        if (savedInstanceState == null) {
            mAppAuth = getArguments().getParcelable(AppDetailActivity.APP_AUTH_KEY);
        } else {
            mAppAuth = savedInstanceState.getParcelable(AppDetailActivity.APP_AUTH_KEY);
        }

        if (mAppAuth == null) {
            throw new IllegalStateException("AppAuth not found");
        }

        String[] services = SPF.get().getServiceRegistry().getServicesOfApp(mAppAuth.getAppIdentifier());
        serviceList.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, services));
        serviceList.setEmptyView(getView().findViewById(R.id.app_manager_services_list_emptyview));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(AppDetailActivity.APP_AUTH_KEY, mAppAuth);
    }
}
