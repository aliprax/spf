package it.polimi.spf.app.navigation;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

import it.polimi.spf.app.MainActivity;
import it.polimi.spf.app.R;
import it.polimi.spf.framework.SPF;
import it.polimi.spf.framework.SPFApp;

/**
 * Component that handles the navigation of {@link MainActivity}
 *
 * @author darioarchetti
 */
public class Navigation implements SPFApp.OnEventListener {

    public static enum Entry {
        PROFILE, PERSONAS, CONTACTS, NOTIFICATIONS, ADVERTISING, APPS, ACTIVITIES;
    }

    private final Context mContext;
    private final String[] mPageTitles;
    private final Map<Entry, NavigationEntry> mEntries;

    public Navigation(Context context) {
        this.mContext = context;
        this.mPageTitles = mContext.getResources().getStringArray(R.array.content_fragments_titles);
        this.mEntries = new HashMap<Entry, NavigationEntry>();
    }

    public View createEntryView(Entry entry) {
        NavigationEntry entryView = new NavigationEntry(mContext);
        entryView.setName(mPageTitles[entry.ordinal()]);
        updateViewNotification(entry, entryView);
        mEntries.put(entry, entryView);
        return entryView;
    }

    @Override
    public void onEvent(int eventCode, Bundle payload) {
        Navigation.Entry entry;

        switch (eventCode) {
            case SPFApp.EVENT_ADVERTISING_STATE_CHANGED: {
                entry = Entry.ADVERTISING;
                break;
            }
            case SPFApp.EVENT_NOTIFICATION_MESSAGE_RECEIVED: {
                entry = Entry.NOTIFICATIONS;
                break;
            }
            case SPFApp.EVENT_CONTACT_REQUEST_RECEIVED: {
                entry = Entry.CONTACTS;
                break;
            }
            default:
                return;
        }

        NavigationEntry entryView = mEntries.get(entry);
        updateViewNotification(entry, entryView);
    }

    private void updateViewNotification(Navigation.Entry entry, NavigationEntry entryView) {
        switch (entry) {
            case NOTIFICATIONS:
                int notifCount = SPF.get().getNotificationManager().getAvailableNotificationCount();
                showNotificationOrClear(entryView, notifCount > 0 ? String.valueOf(notifCount) : null);
                break;
            case CONTACTS:
                int msgCount = SPF.get().getSecurityMonitor().getPersonRegistry().getPendingRequestCount();
                showNotificationOrClear(entryView, msgCount > 0 ? String.valueOf(msgCount) : null);
                break;
            case ADVERTISING:
                boolean active = SPF.get().getAdvertiseManager().isAdvertisingEnabled();
                showNotificationOrClear(entryView, active ? "ON" : null);
                break;
            default:
                break;
        }
    }

    private void showNotificationOrClear(NavigationEntry entry, String text) {
        if (entry == null) {
            return;
        }

        if (text == null) {
            entry.clearNotification();
        } else {
            entry.showNotification(text);
        }
    }
}
