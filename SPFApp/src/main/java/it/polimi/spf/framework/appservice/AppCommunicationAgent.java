package it.polimi.spf.framework.appservice;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

public class AppCommunicationAgent {

    private static final String TAG = "AppCommunicationAgent";
    private Map<String, AppServiceProxy> mProxies;
    private Context mContext;
    private boolean mShutdown;

    public AppCommunicationAgent(Context context) {
        mContext = context;
        mProxies = new HashMap<String, AppServiceProxy>();
    }

    public AppServiceProxy getProxy(String appIntent) {
        synchronized (this) {

            if (mShutdown) {
                throw new IllegalStateException("Communication agent is shutdown");
            }

            if (mProxies.containsKey(appIntent)) {
               AppServiceProxy fromChache = mProxies.get(appIntent);
                if(fromChache.isConnected()){
                    Log.v(TAG, "Alive proxy to " + appIntent + " returned from cache");
                    return fromChache;
                } else {
                    Log.v(TAG, "Dead proxy to " + appIntent + " found in cache. Creating new one");
                }
            }

            AppServiceProxy proxy = new AppServiceProxy(appIntent);
            mProxies.put(appIntent, proxy); // This replaces the old proxy, if any
            Intent serviceIntent = new Intent(appIntent);
            boolean bound = mContext.bindService(serviceIntent, proxy, Context.BIND_AUTO_CREATE);

            if (!bound) {
                Log.e(TAG, "Cannot bound to app service with intent " + appIntent);
            }

            return proxy;
        }
    }

    /**
     * Unbinds from all active app connections and prevents the creation of new ones.
     */
    public void shutdown() {
        synchronized (this) {
            if (mShutdown) {
                return;
            }

            for (AppServiceProxy p : mProxies.values()) {
                if (p.isConnected()) {
                    mContext.unbindService(p);
                }
            }

            mShutdown = true;
        }
    }

    public boolean isShutDown() {
        return mShutdown;
    }
}
