package it.polimi.spf.framework.appservice;

import android.content.Context;
import android.util.Log;

import java.util.Collection;

import it.polimi.spf.framework.Utils;
import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.SPFActivity;
import it.polimi.spf.shared.model.SPFServiceDescriptor;

/**
 *
 *
 */
public class ServiceRegistry {

    private ActivityConsumerRouteTable mActivityTable;
    private ServiceRegistryTable mServiceTable;
    private AppCommunicationAgent mCommunicationAgent;

    public ServiceRegistry(Context context) {
        mServiceTable = new ServiceRegistryTable(context);
        mActivityTable = new ActivityConsumerRouteTable(context);
        mCommunicationAgent = new AppCommunicationAgent(context);
    }

    /**
     * Registers a service. The owner application must be already registered.
     *
     * @param descriptor
     *
     * @return true if the service was registered
     */
    public boolean registerService(SPFServiceDescriptor descriptor) {
        return mServiceTable.registerService(descriptor) && mActivityTable.registerService(descriptor);
    }

    /**
     * Unregisters a service.
     *
     * @param descriptor - the descriptor of the service to unregister
     *
     * @return true if the service was removed
     */
    public boolean unregisterService(SPFServiceDescriptor descriptor) {
        return mServiceTable.unregisterService(descriptor) && mActivityTable.unregisterService(descriptor);
    }

    /**
     * Unregisters all the service of an application
     *
     * @param appIdentifier - the identifier of the app whose service to remove
     *
     * @return true if all the services where removed.
     */
    public boolean unregisterAllServicesOfApp(String appIdentifier) {
        return mServiceTable.unregisterAllServicesOfApp(appIdentifier) && mActivityTable.unregisterAllServicesOfApp(appIdentifier);
    }

    /**
     * Retrieves all the services of an app.
     *
     * @param appIdentifier - the id of the app whose service to retrieve
     *
     * @return the list of its services
     */
    public String[] getServicesOfApp(String appIdentifier) {
        return mServiceTable.getServicesOfApp(appIdentifier);
    }

    /**
     * Dispatches an invocation request to the right application. If the application is not found,
     * an error response is returned.
     *
     * @param request
     *
     * @return
     */
    public InvocationResponse dispatchInvocation(InvocationRequest request) {
        try {
            String appName = request.getAppName();
            String serviceName = request.getServiceName();
            String intent = mServiceTable.getIntentForService(appName, serviceName);
            InvocationResponse resp;

            if (intent == null) {
                resp = InvocationResponse.error("Application " + appName + " doesn't have a service named " + serviceName);
            } else {
                resp = mCommunicationAgent.getProxy(intent).executeService(request);
            }

            Utils.logCall("ServiceRegistry", "Response: " + resp);
            return resp;
        } catch (Throwable t) {
            Log.e("ServiceRegistry", "Error dispatching invocation: ", t);
            return InvocationResponse.error("Internal error: " + t.getMessage());
        }
    }

    /**
     * Dispatches an activity to the right application according to {@link
     * ActivityConsumerRouteTable#}
     *
     * @param activity
     *
     * @return
     */
    public InvocationResponse sendActivity(SPFActivity activity) {
        ServiceIdentifier id = mActivityTable.getServiceFor(activity);
        String intent = mServiceTable.getIntentForService(id);
        InvocationResponse resp;

        if (intent == null) {
            resp = InvocationResponse.error("No service to handle " + activity);
        } else {
            resp = mCommunicationAgent.getProxy(intent).sendActivity(activity);
        }

        Utils.logCall("ServiceRegistry", "Response: " + resp);
        return resp;
    }

    public Collection<VerbSupportDescriptor> getVerbSupport() {
        return mActivityTable.getVerbSupport();
    }

    public void setDefaultConsumerForVerb(String verb, ServiceIdentifier identifier) {
        mActivityTable.setDefaultServiceForVerb(verb, identifier);
    }
}
