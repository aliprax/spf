package it.polimi.spf.framework.security;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;

import java.util.Locale;

import it.polimi.spf.app.R;
import it.polimi.spf.shared.model.Permission;

public class PermissionHelper {

    private static final String TAG = "PermissionHelper";
    private static SparseArray<String> sPermissionNamesCache = new SparseArray<String>();

    public static Permission[] getPermissions(int code) {
        int numberOfPermissions = bitCount(code), index = 0;
        Permission[] permissions = new Permission[numberOfPermissions];
        for (Permission p : Permission.values()) {
            if ((p.getCode() & code) > 0) {
                permissions[index++] = p;
            }
        }
        return permissions;
    }

    public static String getPermissionFriendlyName(Permission p, Context context) {
        String permissionIdentifier = p.name().toLowerCase(Locale.US);
        int id = -1;
        try {
            id = R.string.class.getField("permission_" + permissionIdentifier).getInt(null);
        } catch (IllegalAccessException e) {
        } catch (IllegalArgumentException e) {
        } catch (NoSuchFieldException e) {
        }

        if (id == -1) {
            Log.w(TAG, "No friendly name defined for permission " + permissionIdentifier);
            return permissionIdentifier;
        }

        String name = sPermissionNamesCache.get(id);
        if (name == null) {
            name = context.getString(id);
            sPermissionNamesCache.put(id, name);
        }

        return name;
    }

    /**
     * Constant time, constant memort algorithm to count the number of 1's in a binary
     * representation
     *
     * @param - the number
     *
     * @return the number of 1's in the binary representation
     *
     * @see <a href="http://blogs.msdn.com/b/jeuge/archive/2005/06/08/hakmem-bit-count.aspx">this
     * link on MSDN</a>
     */
    private static int bitCount(int u) {
        int uCount;

        uCount = u - ((u >> 1) & 033333333333) - ((u >> 2) & 011111111111);
        return ((uCount + (uCount >> 3)) & 030707070707) % 63;
    }

}
