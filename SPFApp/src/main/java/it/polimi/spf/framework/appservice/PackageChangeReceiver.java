package it.polimi.spf.framework.appservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import it.polimi.spf.framework.SPF;
import it.polimi.spf.framework.security.SPFSecurityMonitor;

public class PackageChangeReceiver extends BroadcastReceiver {

    private static final String TAG = "PackageChangeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        SPFSecurityMonitor securityMonitor = SPF.get().getSecurityMonitor();

        if (action.equals(Intent.ACTION_PACKAGE_ADDED)) {
            // TODO handle this in future
        } else if (action.equals(Intent.ACTION_PACKAGE_REMOVED)) {
            String removedAppIdentifier = intent.getData().getSchemeSpecificPart();
            if (securityMonitor.isAppRegistered(removedAppIdentifier)) {
                securityMonitor.unregisterApplication(removedAppIdentifier);
                Log.v(TAG, "App " + removedAppIdentifier + " uninstalled from SPF");
            } else {
                Log.v(TAG, "App " + removedAppIdentifier + " was not installed in SPF");
            }
        }
    }
}
