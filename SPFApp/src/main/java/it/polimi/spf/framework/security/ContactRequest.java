package it.polimi.spf.framework.security;

import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import it.polimi.spf.framework.SPF;
import it.polimi.spf.framework.profile.SPFPersona;
import it.polimi.spf.framework.profile.SPFProfileManager;
import it.polimi.spf.framework.proximity.FieldContainerMarshaller;
import it.polimi.spf.shared.model.ProfileField;
import it.polimi.spf.shared.model.ProfileFieldContainer;

public class ContactRequest {

    private static final String TOKEN = "token";
    private static final String CONTAINER = "container";

    private static final String TAG = "FriendshipMessage";

    // Fields to marshall
    private final String mReceiveToken;
    private final ProfileFieldContainer mContainer;

    private ContactRequest(String receiveToken, ProfileFieldContainer container) {
        this.mReceiveToken = receiveToken;
        this.mContainer = container;
    }

    public String getAccessToken() {
        return mReceiveToken;
    }

    public String getUserIdentifier() {
        return mContainer.getFieldValue(ProfileField.IDENTIFIER);
    }

    public String getDisplayName() {
        return mContainer.getFieldValue(ProfileField.DISPLAY_NAME);
    }

    public Bitmap getProfilePicture() {
        return mContainer.getFieldValue(ProfileField.PHOTO);
    }

    public static ContactRequest fromJSON(String friendshipMessageJSON) {
        try {
            JSONObject json = new JSONObject(friendshipMessageJSON);
            String token = json.getString(TOKEN);
            ProfileFieldContainer container = FieldContainerMarshaller.unmarshallContainer(json.getString(CONTAINER));

            return new ContactRequest(token, container);
        } catch (JSONException e) {
            Log.e(TAG, "Error unmarshalling friendship message", e);
            return null;
        }

    }

    public static ContactRequest create(String token) {
        SPFProfileManager profile = SPF.get().getProfileManager();
        ProfileFieldContainer container = profile.getProfileFieldBulk(
                SPFPersona.DEFAULT, ProfileField.IDENTIFIER, ProfileField.DISPLAY_NAME, ProfileField.PHOTO);
        return new ContactRequest(token, container);
    }

    public String toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put(TOKEN, mReceiveToken);
            json.put(CONTAINER, FieldContainerMarshaller.marshallContainer(mContainer));
        } catch (JSONException e) {
            Log.e(TAG, "Json error marshalling friendship message:", e);
        }

        return json.toString();
    }
}
