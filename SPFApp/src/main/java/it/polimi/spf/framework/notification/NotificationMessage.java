package it.polimi.spf.framework.notification;

import android.os.Parcel;
import android.os.Parcelable;

public class NotificationMessage implements Parcelable {

    private final long mId;
    private final String mSender;
    private final String mTitle;
    private final String mMessage;

    public NotificationMessage(long id, String sender, String title, String message) {
        this.mId = id;
        this.mSender = sender;
        this.mTitle = title;
        this.mMessage = message;
    }

    public NotificationMessage(String sender, String title, String message) {
        this(-1, sender, title, message);
    }

    private NotificationMessage(Parcel source) {
        mId = source.readLong();
        mSender = source.readString();
        mTitle = source.readString();
        mMessage = source.readString();
    }

    public long getId() {
        return mId;
    }

    public String getSender() {
        return mSender;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getMessage() {
        return mMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mSender);
        dest.writeString(mTitle);
        dest.writeString(mSender);
    }

    public static final Parcelable.Creator<NotificationMessage> CREATOR = new Creator<NotificationMessage>() {

        @Override
        public NotificationMessage[] newArray(int size) {
            return new NotificationMessage[size];
        }

        @Override
        public NotificationMessage createFromParcel(Parcel source) {
            return new NotificationMessage(source);

        }
    };

}
