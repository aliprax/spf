package it.polimi.spf.framework.appservice;

/**
 * The identifier for a service, made up of the identifier of the owner app, and its name
 *
 * @author darioarchetti
 */
public class ServiceIdentifier {

    private final String mAppId;
    private final String mServiceName;

    public ServiceIdentifier(String appId, String serviceName) {
        this.mAppId = appId;
        this.mServiceName = serviceName;
    }

    public String getAppId() {
        return mAppId;
    }

    public String getServiceName() {
        return mServiceName;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof ServiceIdentifier)) {
            return false;
        }

        ServiceIdentifier other = (ServiceIdentifier) o;
        return other.mAppId.equals(mAppId) &&
                other.mServiceName.equals(mServiceName);
    }

    public int hashCode() {
        return (mAppId + mServiceName).hashCode();
    }

    ;
}
