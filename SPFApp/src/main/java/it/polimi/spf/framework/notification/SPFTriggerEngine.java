/**
 *
 */
package it.polimi.spf.framework.notification;

import android.util.LongSparseArray;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import it.polimi.spf.shared.model.SPFQuery;
import it.polimi.spf.shared.model.SPFTrigger;


/**
 * @author Jacopo Aliprandi
 *
 *         Contains all the triggers' instances. Handle the 'event processing'. Calls action
 *         performer when a trigger has been fired.
 *
 *         IMPORTANT it is not thread safe: to be called in the same handler/thread
 */
public class SPFTriggerEngine {

    private LongSparseArray<SPFTrigger> triggers;
    private SPFActionPerformer actionPerformer;

    public SPFTriggerEngine(SPFActionPerformer performer) {
        this.triggers = new LongSparseArray<SPFTrigger>();
        this.actionPerformer = performer;

    }

    void remove(Long obj) {
        triggers.remove(obj);
    }

    void lookForMatchingTrigger(SPFAdvProfile profile) {
        for (int i = 0; i < triggers.size(); i++) {
            SPFTrigger trg = triggers.valueAt(i);
            boolean res = analize(trg.getQuery(), profile);
            if (res) {
                actionPerformer.perform(profile, trg);
            }
        }
    }

    // TODO refactor in an unique component e.g. query.match(profile)?
    private boolean analize(SPFQuery query, SPFAdvProfile profile) {
        // TODO fix collections: now i use contains(...) that works but is
        // inefficient
        Map<String, String> fields = query.getProfileFields();
        for (String identifier : fields.keySet()) {
            String profileValue = profile.getField(identifier).toLowerCase(Locale.US);
            String queryValue = fields.get(identifier).toLowerCase(Locale.US);
            if (!profileValue.contains(queryValue)) {
                return false;
            }
        }

        for (String tag : query.getTags()) {
            if (!checkTag(profile, tag)) {
                return false;
            }
        }

        return true;
    }

    private boolean checkTag(SPFAdvProfile profile, String tag) {
        String _tag = tag.trim().toLowerCase(Locale.US);
        for (String value : profile.getFieldsValues()) {
            if (value.toLowerCase(Locale.US).contains(_tag)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a new trigger. If exists a trigger with the same id of the one specified as parameter,
     * it will be replaced.
     *
     * @param trigger the trigger to add
     */
    public void put(SPFTrigger trigger) {
        triggers.put(trigger.getId(), trigger);
    }

    /**
     * Update the current set of triggers with the given list.
     *
     * @param triggers2
     */
    public void refreshTriggers(List<SPFTrigger> triggers2) {
        triggers.clear();
        for (SPFTrigger trg : triggers2) {
            triggers.put(trg.getId(), trg);
        }
    }

}
