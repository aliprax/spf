package it.polimi.spf.framework.security;

import java.util.ArrayList;
import java.util.List;

public class PersonAuth {

    private final String uuid;
    private final List<String> circles;

    /* package */PersonAuth(String uuid, List<String> circles) {
        this.circles = circles;
        this.uuid = uuid;
    }

    /**
     * Create an empty PersonAuth with only public circle permission.
     */
    /* package */PersonAuth(String uuid) {
        this.uuid = uuid;
        this.circles = new ArrayList<String>();
    }

    public String getUserIdentifier() {
        return uuid;
    }

    public boolean isAllowed(String circle) {
        return circles.contains(circle);
    }

    public List<String> getCircles() {
        return new ArrayList<String>(circles);
    }

    public static PersonAuth getPublicAuth() {
        List<String> circles = new ArrayList<String>();
        circles.add("public");
        return new PersonAuth(null, circles);
    }
}
