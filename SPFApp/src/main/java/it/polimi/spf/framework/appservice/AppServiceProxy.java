package it.polimi.spf.framework.appservice;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import it.polimi.spf.shared.aidl.ClientExecutionService;
import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.SPFActivity;

public class AppServiceProxy implements ServiceConnection, ClientExecutionService {

    private final static String TAG = "AppProxy";

    private ClientExecutionService mAppService;
    private final String mAppIntent;

    public AppServiceProxy(String appIntent) {
        mAppIntent = appIntent;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        synchronized (this) {
            mAppService = ClientExecutionService.Stub.asInterface(service);
            ((Object) this).notifyAll();
            Log.d(getTag(), "Connected to app service");
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        synchronized (this) {
            mAppService = null;
            Log.d(getTag(), "Disconnected from app service");
        }
    }

    public boolean isConnected() {
        return mAppService != null;
    }

    @Override
    public InvocationResponse executeService(InvocationRequest request) {
        try {
            return getAppService().executeService(request);
        } catch (RemoteException e) {
            Log.e(getTag(), "Remote exception @ executeService", e);
            return InvocationResponse.error(e.getCause());
        }
    }

    @Override
    public InvocationResponse sendActivity(SPFActivity activity) {
        try {
            return getAppService().sendActivity(activity);
        } catch (RemoteException e) {
            Log.e(getTag(), "Remmote exception @ sendActivity", e);
            return InvocationResponse.error(e.getCause());
        }
    }

    private ClientExecutionService getAppService() {
        synchronized (this) {
            while (mAppService == null) {
                try {
                    ((Object) this).wait();
                } catch (InterruptedException e) {
                    Log.e(getTag(), "InterruptedException", e);
                }
            }

            return mAppService;
        }

    }

    private String getTag() {
        return TAG + "#" + mAppIntent;
    }

    @Override
    public IBinder asBinder() {
        throw new RuntimeException("Do not call asBinder on proxy");
    }
}
