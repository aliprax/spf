package it.polimi.spf.framework;

import android.content.Context;

import java.util.Random;

import it.polimi.spf.framework.appservice.ServiceRegistry;
import it.polimi.spf.framework.local.SPFService;
import it.polimi.spf.framework.notification.SPFAdvertisingManager;
import it.polimi.spf.framework.notification.SPFNotificationManager;
import it.polimi.spf.framework.people.PeopleManager;
import it.polimi.spf.framework.profile.SPFPersona;
import it.polimi.spf.framework.profile.SPFProfileManager;
import it.polimi.spf.framework.proximity.InboundProximityInterface;
import it.polimi.spf.framework.proximity.InboundProximityInterfaceImpl;
import it.polimi.spf.framework.proximity.ProximityMiddleware;
import it.polimi.spf.framework.search.SearchManager;
import it.polimi.spf.framework.security.SPFSecurityMonitor;
import it.polimi.spf.shared.model.BaseInfo;
import it.polimi.spf.shared.model.ProfileField;
import it.polimi.spf.shared.model.ProfileFieldContainer;

/**
 *
 *
 */
public class SPF {

    private static SPF singleton;

    /**
     * Initializes SPF with a {@link Context} reference. Automatically called during by {@link
     * SPFApp}. Calling it again does nothing
     *
     * @param context
     */
    /* package */
    synchronized static void initialize(Context context) {
        if (singleton == null) {
            singleton = new SPF(context);
        }
    }

    /**
     * Obtains a reference to the SPF singleton.
     *
     * @return the SPF singleton.
     */
    public static synchronized SPF get() {
        return singleton;
    }

    private Context mContext;
    private String uniqueIdentifier;

    // External interfaces
    private ProximityMiddleware mMiddleware;
    private SPFService spfService = null;

    // Components
    private SPFSecurityMonitor mSecurityMonitor;
    private PeopleManager mPeopleManager;
    private SPFProfileManager mProfileManager;
    private ServiceRegistry mServiceRegistry;
    private SearchManager mSearchManager;
    private SPFNotificationManager mNotificationManager;
    private SPFAdvertisingManager mAdvertiseManager;

    private SPF(Context context) {
        mContext = context;

        // Initialize components
        mServiceRegistry = new ServiceRegistry(context);
        mPeopleManager = new PeopleManager();
        mProfileManager = new SPFProfileManager(context);
        mSearchManager = new SearchManager();
        mNotificationManager = new SPFNotificationManager();
        mSecurityMonitor = new SPFSecurityMonitor(context);

        // unique id generation
        ProfileFieldContainer pfc = mProfileManager.getProfileFieldBulk(SPFPersona.getDefault(), ProfileField.IDENTIFIER);
        uniqueIdentifier = pfc.getFieldValue(ProfileField.IDENTIFIER);
        if (uniqueIdentifier == null) {// TODO move out
            uniqueIdentifier = "U" + ((int) (new Random().nextFloat() * 10000));
            pfc.setFieldValue(ProfileField.IDENTIFIER, uniqueIdentifier);
        }
        mProfileManager.setProfileFieldBulk(pfc, SPFPersona.getDefault());

        // Initialize middleware
        InboundProximityInterface proximityInterface = new InboundProximityInterfaceImpl(this);
        mMiddleware = ProximityMiddleware.Factory.newInstance(mContext, proximityInterface, uniqueIdentifier);

        mAdvertiseManager = new SPFAdvertisingManager(context, mMiddleware);
    }

    // Utility getters
    public Context getContext() {
        return mContext;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    // Life-cycle methods
    public void connect() {
        if (!mMiddleware.isConnected()) {
            mMiddleware.connect();
        }

        if (!mNotificationManager.isRunning()) {
            mNotificationManager.start();
        }

        if (mAdvertiseManager.isAdvertisingEnabled()) {
            mMiddleware.registerAdvertisement(mAdvertiseManager.generateAdvProfile().toJSON(), 10000);
        }
    }

    public void disconnect() {
        if (mMiddleware.isAdvertising()) {
            mMiddleware.unregisterAdvertisement();
        }

        if (mMiddleware.isConnected()) {
            mMiddleware.disconnect();
        }

        if (mNotificationManager.isRunning()) {
            mNotificationManager.stop();
        }
    }

    public boolean isConnected() {
        return mMiddleware.isConnected();
    }

    // Link for local server service
    public void onServerCreate(SPFService spfService) {
        this.spfService = spfService;
    }

    public void onServerDestroy() {
        spfService = null;
    }

    // Components getters
    public ServiceRegistry getServiceRegistry() {
        return mServiceRegistry;
    }

    public SPFProfileManager getProfileManager() {
        return mProfileManager;
    }

    public SPFSecurityMonitor getSecurityMonitor() {
        return mSecurityMonitor;
    }

    public SearchManager getSearchManager() {
        return mSearchManager;
    }

    public SPFNotificationManager getNotificationManager() {
        return mNotificationManager;
    }

    public SPFAdvertisingManager getAdvertiseManager() {
        return mAdvertiseManager;
    }

    public PeopleManager getPeopleManager() {
        return mPeopleManager;
    }

    // TODO: where are we putting these?
    public void sendSearchSignal(String queryId, String query) {
        mMiddleware.sendSearchSignal(getUniqueIdentifier(), queryId, query);
    }

    public void sendSearchResult(String queryId, BaseInfo baseInfo) {
        mMiddleware.sendSearchResult(queryId, uniqueIdentifier, baseInfo.toJSONString());
    }
}
