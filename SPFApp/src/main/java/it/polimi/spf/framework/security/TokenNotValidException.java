/**
 *
 */
package it.polimi.spf.framework.security;


import it.polimi.spf.shared.model.SPFError;

/**
 * @author Jacopo
 */
public class TokenNotValidException extends Exception {

    private static final long serialVersionUID = 5413270956520005453L;

    public int getSPFErrorCode() {

        return SPFError.TOKEN_NOT_VALID_ERROR_CODE;
    }
}
