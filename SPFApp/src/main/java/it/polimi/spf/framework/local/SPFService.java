/**
 *
 */
package it.polimi.spf.framework.local;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import it.polimi.spf.app.MainActivity;
import it.polimi.spf.app.R;
import it.polimi.spf.framework.SPF;

/**
 *
 */
public class SPFService extends Service {

    private static final String TAG = "SPFService";

    private static final String INTENT_SERVER = "it.polimi.spf.framework.local.SPFServerService";
    private static final String INTENT_PROFILE = "it.polimi.spf.framework.appservice.SPFProfileService";
    private static final String INTENT_SERVICE = "it.polimi.spf.services.LocalServiceExecutor";
    private static final String INTENT_NOTIFICATION = "it.polimi.spf.framework.SPFNotificationService";
    private final static String INTENT_SECURITY = "it.polimi.spf.framework.local.SecurityService";

    private IBinder mServerBinder;
    private IBinder mProfileBinder;
    private IBinder mLocalServiceBinder;
    private IBinder mNotificationBinder;
    private IBinder mSecurityBinder;

    /*
     * (non-Javadoc)
     *
     * @see android.app.Service#onBind(android.content.Intent)
     */
    @Override
    public IBinder onBind(Intent intent) {
        if (intent.getAction().equalsIgnoreCase(INTENT_SERVER)) {
            Log.d(TAG, "External app bound with action " + intent.getAction());
            SPF.get().connect();

            if (mServerBinder == null) {
                mServerBinder = new SPFProximityServiceImpl();
            }

            return mServerBinder;

        } else if (intent.getAction().equalsIgnoreCase(INTENT_PROFILE)) {
            // requests the binder that offers the access to the local profile
            if (mProfileBinder == null) {
                mProfileBinder = new LocalProfileServiceImpl(this);
            }
            return mProfileBinder;

        } else if (intent.getAction().equalsIgnoreCase(INTENT_SERVICE)) {
            // request the binder to manage local services
            if (mLocalServiceBinder == null) {
                mLocalServiceBinder = new LocalServiceManagerImpl();
            }
            return mLocalServiceBinder;

        } else if (intent.getAction().equalsIgnoreCase(INTENT_NOTIFICATION)) {
            // request the binder to access notification services
            if (mNotificationBinder == null) {
                mNotificationBinder = new SPFNotificationServiceImpl(this);
            }
            return mNotificationBinder;
        } else if (intent.getAction().equals(INTENT_SECURITY)) {
            if (mSecurityBinder == null) {
                mSecurityBinder = new SPFSecurityServiceImpl(this);
            }
            return mSecurityBinder;
        }
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!SPF.get().isConnected()) {
            SPF.get().connect();
        }
        startInForeground();
        return START_STICKY;
    }

    private void startInForeground() {
        Notification notification;
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.ic_launcher).setTicker("spf is active").setContentTitle("SPF").setContentText("SPF is active.").setContentIntent(pIntent);
        notification = builder.build();
        startForeground(001, notification);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SPF.get().onServerCreate(this);
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        SPF.get().onServerDestroy();
        SPF.get().disconnect();
    }

}
