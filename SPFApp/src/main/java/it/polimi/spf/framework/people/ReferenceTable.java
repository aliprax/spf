package it.polimi.spf.framework.people;


import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import it.polimi.spf.framework.proximity.SPFRemoteInstance;

public class ReferenceTable {

    private final static String TAG = "ReferenceTable";

    private Map<String, SPFRemoteInstance> references;

    public ReferenceTable() {
        references = new HashMap<String, SPFRemoteInstance>();
    }

    public void addReference(String name, SPFRemoteInstance reference) {
        references.put(name, reference);
        Log.d(TAG, "Added reference " + name);
    }

    public void removeReference(String name) {
        references.remove(name);
        Log.d(TAG, "Removed reference " + name);
    }

    public SPFRemoteInstance getReference(String target) {
        return references.get(target);
    }
}
