package it.polimi.spf.framework.appservice;

import android.provider.BaseColumns;

public class ServiceRegistryTableContract {

    public static class ServiceEntry implements BaseColumns {
        public static final String TABLE_NAME = "service_table";
        public static final String COLUMN_APP_IDENTIFIER = "app_identifier";
        public static final String COLUMN_SERVICE_NAME = "service_name";
        public static final String COLUMN_VERSION = "service_version";
        public static final String COLUMN_INTENT = "intent_name";
    }

}
