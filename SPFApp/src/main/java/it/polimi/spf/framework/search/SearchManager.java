/**
 *
 */
package it.polimi.spf.framework.search;

import it.polimi.spf.shared.aidl.SPFSearchCallback;
import it.polimi.spf.shared.model.SPFSearchDescriptor;

/**
 *
 *
 */
public class SearchManager {

    private SearchPerformerImpl mSearchPerformer;
    private SearchScheduler mSearchScheduler;

    public SearchManager() {
        mSearchPerformer = new SearchPerformerImpl();
        mSearchScheduler = new SearchScheduler(mSearchPerformer);
    }

    /**
     * Call this method to stop the search and release the associated resources. The application
     * will not be notified about the event;
     *
     * @param queryId
     */
    public void stopSearch(String queryId) {
        mSearchScheduler.stopSearch(queryId);
    }

    /**
     * Start a new search.
     *
     * @param appIdentifier    - the package name of the caller app
     * @param searchDescriptor - a descriptor specifying query and settings
     *
     * @return the identifier of the query
     */
    public String startSearch(String appIdentifier,
                              SPFSearchDescriptor searchDescriptor) {
        String queryId = mSearchScheduler.startSearch(appIdentifier,
                searchDescriptor);
        return queryId;
    }

    /**
     * Register a callback to handle asynchronous communication. The appIdentifier must be the same
     * used in the SearchDescriptors: use the package name of the app.
     *
     * @param appIdentifier - the identifier of the app
     * @param callback      - a SPFSearchCallback implementation
     */
    public void registerSearchCallback(String appIdentifier,
                                       SPFSearchCallback callback) {

        mSearchPerformer.registerSearchCallback(callback, appIdentifier);
    }

    /**
     * Unregister the callback associated with the appIdentifier. Active queries associated to the
     * same id are stopped.
     *
     * @param appIdentifier
     *
     * @return
     */
    public boolean unregisterSearchCallback(String appIdentifier) {

        mSearchScheduler.stopAllSearches(appIdentifier);
        return mSearchPerformer.unregisterSearchCallback(appIdentifier);

    }

    /**
     * Return true if a SPFSearchCallback has been already registered for the given appIdentifier.
     *
     * @param appIdentifier
     *
     * @return true if there is an associated callback, otherwise false
     */
    public boolean isSearchCallbackRegistered(String appIdentifier) {

        return mSearchPerformer.isSearchCallbackRegistered(appIdentifier);

    }

    public void onInstanceLost(String uniqueIdentifier) {

        mSearchScheduler.onInstanceLost(uniqueIdentifier);

    }

    public void onSearchResultReceived(SearchResult searchResult) {

        mSearchScheduler.onSearchResultReceived(searchResult);

    }

}
