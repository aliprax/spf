package it.polimi.spf.framework.people;

import it.polimi.spf.framework.proximity.SPFRemoteInstance;


/**
 * TODO implement resource management
 */
public class PeopleManager {

    private ReferenceTable rt = new ReferenceTable();

    public PeopleManager() {

    }

    public SPFRemoteInstance getPerson(String target) {

        return rt.getReference(target);
    }

    public void removePerson(String uniqueIdentifier) {
        rt.removeReference(uniqueIdentifier);
    }

    public void newPerson(SPFRemoteInstance instance) {
        rt.addReference(instance.getUniqueIdentifier(), instance);
        //SPF.get(SPFApp.get()).dispatchSearchResult(instance.getUniqueIdentifier());
    }

    public boolean hasPerson(String identifier) {
        return getPerson(identifier) != null;
    }

}
