package it.polimi.spf.framework.search;

public interface SearchPerformer {

    void sendSearchSignal(QueryInfo queryInfo);

    void notifyStoppedSearch(QueryInfo qd);

    void dispatchSearchResult(QueryInfo qd, SearchResult result);

    void notifyResultLost(QueryInfo qi, String uniqueIdentifier);

}
