package it.polimi.spf.framework;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

public class IdentifierGenerator {

    private final static int ACCESS_TOKEN_LENGTH = 26;
    private Random mRandom = new SecureRandom();

    public String generateIdentifier(int length) {
        return new BigInteger(length * 5, mRandom).toString(32);
    }

    public String generateAccessToken() {
        return generateIdentifier(ACCESS_TOKEN_LENGTH);
    }
}
