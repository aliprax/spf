package it.polimi.spf.framework;

public final class SPFConfig {

    private SPFConfig() {
        // No instances
    }

    public static final boolean IS_X86 = System.getProperty("os.arch").equals("i686");
    public static final boolean DEBUG = false;

}
