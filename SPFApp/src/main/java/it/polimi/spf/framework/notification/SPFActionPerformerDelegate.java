package it.polimi.spf.framework.notification;

import android.content.Intent;
import android.util.Log;

import it.polimi.spf.framework.SPF;
import it.polimi.spf.framework.SPFApp;
import it.polimi.spf.framework.proximity.SPFRemoteInstance;
import it.polimi.spf.shared.model.ProfileField;
import it.polimi.spf.shared.model.SPFAction;
import it.polimi.spf.shared.model.SPFActionIntent;
import it.polimi.spf.shared.model.SPFActionSendNotification;
import it.polimi.spf.shared.model.SPFTrigger;

public class SPFActionPerformerDelegate implements SPFActionPerformer {

    @Override
    public void perform(SPFAdvProfile target, SPFTrigger trigger) {
        SPFAction action = trigger.getAction();
        if (action instanceof SPFActionIntent) {
            performIntentAction(target, trigger, (SPFActionIntent) action);
        } else if (action instanceof SPFActionSendNotification) {
            performSendNotificationAction(target, (SPFActionSendNotification) action);
        }
    }

    private void performSendNotificationAction(SPFAdvProfile target, SPFActionSendNotification action) {
        Log.d("NOTIFICATION!!", action.getTitle() + ": " + action.getMessage());
        String targetId = target.getField(ProfileField.IDENTIFIER.getIdentifier());
        SPFRemoteInstance targetInstance = SPF.get().getPeopleManager().getPerson(targetId);
        if (targetInstance == null) {
            return;
        } else {

            targetInstance.sendNotification(SPF.get().getUniqueIdentifier(), action);
        }
    }

    private void performIntentAction(SPFAdvProfile target, SPFTrigger trigger, SPFActionIntent action) {
        String actionString = action.getAction();
        Intent intent = new Intent(actionString);
        intent.putExtra(SPFActionIntent.ARG_STRING_TARGET, target.getField(ProfileField.IDENTIFIER.getIdentifier()));
        intent.putExtra(SPFActionIntent.ARG_STRING_DISPLAY_NAME, target.getField(ProfileField.DISPLAY_NAME.getIdentifier()));
        intent.putExtra(SPFActionIntent.ARG_STRING_TRIGGER_NAME, trigger.getName());
        intent.putExtra(SPFActionIntent.ARG_LONG_TRIGGER_ID, trigger.getId());
        SPFApp.get().sendBroadcast(intent);
    }

}
