package it.polimi.spf.framework.appservice;

import java.util.HashSet;
import java.util.Set;

public class VerbSupportDescriptor {

    private final String mVerb;
    private Set<ServiceIdentifier> mSupportingServices;
    private ServiceIdentifier mDefaultService;

    public VerbSupportDescriptor(String verb) {
        mSupportingServices = new HashSet<ServiceIdentifier>();
        mVerb = verb;
    }

    public String getVerb() {
        return mVerb;
    }

    public Set<ServiceIdentifier> getSupportingServices() {
        return mSupportingServices;
    }

    public void addSupportingService(ServiceIdentifier identifier) {
        mSupportingServices.add(identifier);
    }

    public ServiceIdentifier getDefaultService() {
        return mDefaultService;
    }

    public void setDefaultApp(ServiceIdentifier service) {
        this.mDefaultService = service;
    }
}
