/**
 *
 */
package it.polimi.spf.framework;

import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * {@link Application} subclass that serves two main purposes: <ul> <li>Initializing SPF singleton
 * with a context reference</li> <li>Local broadcaster for application-wide events</li> <ul>
 *
 * @author aliprax
 */
@SuppressWarnings("unused")
public class SPFApp extends Application {

    // Events
    public static final int EVENT_NOTIFICATION_MESSAGE_RECEIVED = 0;
    public static final int EVENT_CONTACT_REQUEST_RECEIVED = 1;
    public static final int EVENT_ADVERTISING_STATE_CHANGED = 2;

    // Payload extra keys
    public static final String EXTRA_NOTIFICATION_MESSAGE = "notification_message";
    public static final String EXTRA_ACTIVE = "active";

    // Private constants
    private static SPFApp singleton;
    private static final String TAG = "SPFApp";

    // Load alljoyn library only for arm architectures as we still haven't the
    // x86 version.
    static {
        if (SPFConfig.IS_X86 && SPFConfig.DEBUG) {
            Log.w(TAG, "Alljoyn library not loaded");
        } else {
            System.loadLibrary("alljoyn_java");
        }
    }

    private Set<OnEventListener> mEventListeners;
    private Handler mHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;

        // Set the default exception handler
        ExceptionLogger.installAsDefault(this);

        // Initialize SPF
        SPF.initialize(this);

        // Initialize event broadcaster
        mEventListeners = Collections.newSetFromMap(new ConcurrentHashMap<OnEventListener, Boolean>());
        mHandler = new Handler(getMainLooper());

    }

    public static SPFApp get() {
        return singleton;
    }

    /**
     * Interface for components to listen to events broadcasted by {@link SPFApp}. It is possible to
     * register a broadcast listener using
     *
     * @author darioarchetti
     */
    public static interface OnEventListener {

        /**
         * Called when an event is broadcasted through {@link SPFApp}. This call happens on the main
         * thread.
         *
         * @param eventCode - the code of the event.
         * @param payload   - a Bundle containing event-specific information.
         */
        void onEvent(int eventCode, Bundle payload);
    }

    /**
     * Registers a listener to be notified when an event is broadcasted.
     *
     * @param listener - the listener to register.
     */
    public void registerEventListener(OnEventListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener == null");
        }

        mEventListeners.add(listener);
    }

    /**
     * Unregisters a listener.
     *
     * @param listener - the listener to unregister
     */
    public void unregisterEventListener(OnEventListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener == null");
        }

        mEventListeners.add(listener);
    }

    /**
     * Broadcasts an event to all registered listeners.
     *
     * @param code - the event code
     */
    public void broadcastEvent(int code) {
        broadcastEvent(code, null);
    }

    /**
     * Broadcasts an event to all registered listeners. Each listener will receive a reference to
     * the given bundle.
     *
     * @param code    - the event code
     * @param payload - the event payload
     */
    public void broadcastEvent(final int code, final Bundle payload) {
        if (SPFConfig.DEBUG) {
            Log.d(TAG, "Broadcasting event " + code + " with payload " + payload);
        }

        for (final OnEventListener listener : mEventListeners) {
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    listener.onEvent(code, payload);
                }
            });
        }
    }
}
