package it.polimi.spf.framework.proximity;

// Dummy implementation of middleware loaded when SPF is run on a x86 platform
public class DummyMiddleware implements ProximityMiddleware {

    private boolean isAdvertising = false;
    private boolean isConnected = false;

    @Override
    public void connect() {
        this.isConnected = true;
    }

    @Override
    public void disconnect() {
        this.isConnected = false;
    }

    @Override
    public boolean isConnected() {
        return isConnected;
    }

    @Override
    public void sendSearchResult(String id, String uniqueIdentifier, String baseInfo) {

    }

    @Override
    public void sendSearchSignal(String sender, String searchId, String query) {

    }

    @Override
    public void registerAdvertisement(String e, long sendPeriod) {
        this.isAdvertising = true;
    }

    @Override
    public void unregisterAdvertisement() {
        this.isAdvertising = false;
    }

    @Override
    public boolean isAdvertising() {
        return isAdvertising;
    }

}
