package it.polimi.spf.framework.appservice;

import it.polimi.spf.framework.SPF;
import it.polimi.spf.framework.profile.SPFPersona;
import it.polimi.spf.shared.model.ProfileField;
import it.polimi.spf.shared.model.ProfileFieldContainer;
import it.polimi.spf.shared.model.SPFActivity;

public class ActivityInjector {

    public static void injectDataInActivity(SPFActivity activity, String targetId) {
        ProfileFieldContainer pfc = SPF.get().getProfileManager().getProfileFieldBulk(SPFPersona.DEFAULT, ProfileField.IDENTIFIER, ProfileField.DISPLAY_NAME);

        activity.put(SPFActivity.SENDER_DISPLAY_NAME, pfc.getFieldValue(ProfileField.DISPLAY_NAME));
        activity.put(SPFActivity.SENDER_IDENTIFIER, pfc.getFieldValue(ProfileField.IDENTIFIER));

        // TODO ActivityStreams inject information of the recipient
    }

    public static void injectDataInActivity(SPFActivity activity) {
        ProfileFieldContainer pfc = SPF.get().getProfileManager().getProfileFieldBulk(SPFPersona.DEFAULT, ProfileField.IDENTIFIER, ProfileField.DISPLAY_NAME);

        activity.put(SPFActivity.SENDER_DISPLAY_NAME, pfc.getFieldValue(ProfileField.DISPLAY_NAME));
        activity.put(SPFActivity.SENDER_IDENTIFIER, pfc.getFieldValue(ProfileField.IDENTIFIER));
        activity.put(SPFActivity.RECEIVER_DISPLAY_NAME, pfc.getFieldValue(ProfileField.DISPLAY_NAME));
        activity.put(SPFActivity.RECEIVER_IDENTIFIER, pfc.getFieldValue(ProfileField.IDENTIFIER));
    }

}
