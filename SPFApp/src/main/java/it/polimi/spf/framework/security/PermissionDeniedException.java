package it.polimi.spf.framework.security;


import it.polimi.spf.shared.model.SPFError;

public class PermissionDeniedException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 4121426626961038483L;

    public int getSPFErrorCode() {

        return SPFError.PERMISSION_DENIED_ERROR_CODE;
    }

}
