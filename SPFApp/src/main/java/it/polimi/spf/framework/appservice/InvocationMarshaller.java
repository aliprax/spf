package it.polimi.spf.framework.appservice;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.SPFActivity;

public class InvocationMarshaller {

    public static JsonElement toJsonElement(InvocationRequest invocationRequest) {
        return new Gson().toJsonTree(invocationRequest);
    }

    public static InvocationRequest requestfromJsonElement(JsonElement invocationRequest) {
        return new Gson().fromJson(invocationRequest, InvocationRequest.class);
    }

    public static String toJson(InvocationRequest request) {
        return new Gson().toJson(request);
    }

    public static InvocationRequest requestFromJson(String request) {
        return new Gson().fromJson(request, InvocationRequest.class);
    }

    public static JsonElement toJsonElement(InvocationResponse invocationResponse) {
        return new Gson().toJsonTree(invocationResponse);
    }

    public static InvocationResponse responsefromJsonElement(JsonElement invocationResponse) {
        return new Gson().fromJson(invocationResponse, InvocationResponse.class);
    }

    public static String toJson(InvocationResponse response) {
        return new Gson().toJson(response);
    }

    public static InvocationResponse responsefromJson(String response) {
        return new Gson().fromJson(response, InvocationResponse.class);
    }

    public static JsonElement toJsonElement(SPFActivity activity) {
        return new Gson().toJsonTree(activity);
    }

    public static SPFActivity activityFromJsonElement(JsonElement activity) {
        return new Gson().fromJson(activity, SPFActivity.class);
    }

    public static String toJson(SPFActivity activity) {
        return new Gson().toJson(activity);
    }

    public static SPFActivity activityFromJson(String json) {
        return new Gson().fromJson(json, SPFActivity.class);
    }
}
