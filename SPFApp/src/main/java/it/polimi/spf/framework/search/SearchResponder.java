package it.polimi.spf.framework.search;

import android.content.Context;

import org.json.JSONException;

import java.util.Map;

import it.polimi.spf.framework.SPF;
import it.polimi.spf.framework.profile.SPFPersona;
import it.polimi.spf.framework.profile.SPFProfileManager;
import it.polimi.spf.framework.security.SPFSecurityMonitor;
import it.polimi.spf.shared.model.ProfileField;
import it.polimi.spf.shared.model.ProfileFieldContainer;
import it.polimi.spf.shared.model.ProfileFieldConverter;
import it.polimi.spf.shared.model.SPFQuery;

/**
 * Class to respond to received queries
 *
 * @author darioarchetti
 */
public class SearchResponder {


    public SearchResponder(Context context) {

    }

    /**
     * Verifies if the local profile matches the given query.
     *
     * @param queryJSON
     *
     * @return
     *
     * @see it.polimi.spf.shared.model.SPFQuery
     */
    public boolean matches(String queryJSON) {
        QueryContainer queryContainer;
        try {
            queryContainer = QueryContainer.fromJSON(queryJSON);
        } catch (JSONException e) {
            return false;
        }

        SPFQuery query = queryContainer.getQuery();
        String callerApp = queryContainer.getCallerAppId();
        String userUID = queryContainer.getUserUID();
        return analyzeWith(query, callerApp, userUID);

    }

    private boolean analyzeWith(SPFQuery query, String callerApp, String userUID) {
        SPFProfileManager mProfile = SPF.get().getProfileManager();
        SPFSecurityMonitor mSecMonitor = SPF.get().getSecurityMonitor();
        SPFPersona persona = mSecMonitor.getPersonaOf(callerApp);

        Map<String, String> fields = query.getProfileFields();
        ProfileField[] queryFields =
                ProfileField.fromIdentifierList(fields.keySet().toArray(new String[fields.keySet().size()]));
        ProfileFieldContainer pfc =
                mProfile.getProfileFieldBulk(persona, queryFields);
        for (ProfileField f : queryFields) {
            //FIXME refactor profile fields conversion vs. storage string
            String myValue = ProfileFieldConverter.forField(f).toStorageString(pfc.getFieldValue(f));
            String queryValue = fields.get(f.getIdentifier());
            if (!myValue.toLowerCase().contains(queryValue.toLowerCase())) {
                //FIXME contains() is a hack for collection
                return false;
            }
        }

        for (String tag : query.getTags()) {
            if (!mProfile.hasTag(tag, persona)) {
                return false;
            }
        }

        for (String app : query.getApps()) {
            if (!mSecMonitor.isAppRegistered(app)) {
                return false;
            }
        }
        return true;

    }

}
