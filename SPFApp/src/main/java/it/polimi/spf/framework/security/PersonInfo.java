package it.polimi.spf.framework.security;

public class PersonInfo {

    private final String mDisplayName;
    private PersonAuth mAuth;
    private boolean mHasPicture;

    public PersonInfo(PersonAuth auth, String displayName, boolean hasPicture) {
        if (auth == null) {
            throw new NullPointerException("auth cannot be null");
        }

        this.mAuth = auth;
        this.mDisplayName = displayName;
        this.mHasPicture = hasPicture;
    }

    public String getIdentifier() {
        return mAuth.getUserIdentifier();
    }

    public String getDisplayName() {
        return mDisplayName != null ? mDisplayName : mAuth.getUserIdentifier();
    }

    public PersonAuth getPersonAuth() {
        return mAuth;
    }

    public boolean hasProfilePic() {
        return mHasPicture;
    }
}
