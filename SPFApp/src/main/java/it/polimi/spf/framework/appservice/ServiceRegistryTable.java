package it.polimi.spf.framework.appservice;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import it.polimi.spf.framework.appservice.ServiceRegistryTableContract.ServiceEntry;
import it.polimi.spf.shared.model.SPFServiceDescriptor;


public class ServiceRegistryTable extends SQLiteOpenHelper {

    public static final String TAG = "ServiceTable";

    // If you change the database schema, you must increment the database
    // version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ServiceTable.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + ServiceEntry.TABLE_NAME + " (" + ServiceEntry._ID + " INTEGER PRIMARY KEY," + ServiceEntry.COLUMN_APP_IDENTIFIER + TEXT_TYPE + COMMA_SEP + ServiceEntry.COLUMN_SERVICE_NAME + TEXT_TYPE + COMMA_SEP + ServiceEntry.COLUMN_VERSION + TEXT_TYPE + COMMA_SEP + ServiceEntry.COLUMN_INTENT + TEXT_TYPE + COMMA_SEP + "UNIQUE ( " + ServiceEntry.COLUMN_APP_IDENTIFIER + COMMA_SEP + ServiceEntry.COLUMN_SERVICE_NAME + ") ON CONFLICT REPLACE )";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + ServiceEntry.TABLE_NAME;

    public ServiceRegistryTable(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public boolean registerService(SPFServiceDescriptor descriptor) {
        SQLiteDatabase db = getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(ServiceEntry.COLUMN_APP_IDENTIFIER, descriptor.getAppIdentifier());
        values.put(ServiceEntry.COLUMN_SERVICE_NAME, descriptor.getServiceName());
        values.put(ServiceEntry.COLUMN_VERSION, descriptor.getVersion());
        values.put(ServiceEntry.COLUMN_INTENT, descriptor.getIntent());

        // Insert the new row, returning the primary key value of the new row
        long newRowId;
        newRowId = db.insert(ServiceEntry.TABLE_NAME, null, values);

        if (newRowId == -1) {
            Log.e(TAG, "Db insert returns -1");
            return false;
        }

        return true;
    }

    public boolean unregisterAllServicesOfApp(String appIdentifier) {
        SQLiteDatabase db = getWritableDatabase();
        // Define 'where' part of query.
        String selection = ServiceEntry.COLUMN_APP_IDENTIFIER + " = ? ";

        // Specify arguments in placeholder order.
        String[] selectionArgs = {appIdentifier};

        // Issue SQL statement.
        return db.delete(ServiceEntry.TABLE_NAME, selection, selectionArgs) > 0;
    }

    public boolean unregisterService(SPFServiceDescriptor descriptor) {
        SQLiteDatabase db = getWritableDatabase();
        // Define 'where' part of query.
        String selection = ServiceEntry.COLUMN_APP_IDENTIFIER + " = ? " + "AND " + ServiceEntry.COLUMN_SERVICE_NAME + "= ? ";

        // Specify arguments in placeholder order.
        String[] selectionArgs = {descriptor.getAppIdentifier(), descriptor.getServiceName()};

        // Issue SQL statement.
        return db.delete(ServiceEntry.TABLE_NAME, selection, selectionArgs) > 0;
    }

    public String getIntentForService(String appName, String serviceName) {
        String where = ServiceEntry.COLUMN_APP_IDENTIFIER + " = ? AND " + ServiceEntry.COLUMN_SERVICE_NAME + " = ?";
        String[] whereArgs = {appName, serviceName};
        String[] columns = {ServiceEntry.COLUMN_INTENT};

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(ServiceEntry.TABLE_NAME, columns, where, whereArgs, null, null, null);

        if (!c.moveToNext()) {
            return null;
        }

        String intent = c.getString(c.getColumnIndexOrThrow(ServiceEntry.COLUMN_INTENT));
        c.close();

        return intent;
    }

    public String getIntentForService(ServiceIdentifier id) {
        if (id == null) {
            return null;
        }

        return getIntentForService(id.getAppId(), id.getServiceName());
    }

    public String[] getServicesOfApp(String appIdentifier) {
        String where = ServiceEntry.COLUMN_APP_IDENTIFIER + " = ?";
        String[] whereArgs = {appIdentifier};
        String[] columns = {ServiceEntry.COLUMN_SERVICE_NAME};
        Cursor c = getReadableDatabase().query(ServiceEntry.TABLE_NAME, columns, where, whereArgs, null, null, null);

        String[] services = new String[c.getCount()];
        int i = 0;
        while (c.moveToNext()) {
            services[i++] = c.getString(c.getColumnIndexOrThrow(ServiceEntry.COLUMN_SERVICE_NAME));
        }

        c.close();
        return services;
    }

    public void deleteTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}
