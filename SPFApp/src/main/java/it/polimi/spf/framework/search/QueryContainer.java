/**
 *
 */
package it.polimi.spf.framework.search;

import org.json.JSONException;
import org.json.JSONObject;

import it.polimi.spf.shared.model.SPFQuery;


/**
 * @author Jacopo Contains all the information needed to perform queries.
 */
class QueryContainer {

    /*
     * The query
     */
    private final SPFQuery query;

    /*
     * The caller app identifier. Used to select the right SPFPersona.
     */
    private final String callerAppId;

    /*
     * The user identifier.
     */
    private final String userUID;


    QueryContainer(SPFQuery query, String callerAppId, String userUID) {
        super();
        this.query = query;
        this.callerAppId = callerAppId;
        this.userUID = userUID;
    }

    String toJSON() {
        JSONObject o = new JSONObject();
        try {
            o.put("query", query.toQueryString())
                    .put("callerAppId", callerAppId)
                    .put("userUID", userUID);
        } catch (JSONException e) {
            return null;
        }
        return o.toString();
    }

    static QueryContainer fromJSON(String json) throws JSONException {
        JSONObject o = new JSONObject(json);
        SPFQuery query = SPFQuery.fromQueryString(o.getString("query"));
        String callerAppId = o.getString("callerAppId");
        String userUID = o.getString("userUID");
        return new QueryContainer(query, callerAppId, userUID);
    }

    /**
     * @return the query
     */
    protected SPFQuery getQuery() {
        return query;
    }

    /**
     * @return the callerAppId
     */
    protected String getCallerAppId() {
        return callerAppId;
    }

    /**
     * @return the userUID
     */
    protected String getUserUID() {
        return userUID;
    }


}
