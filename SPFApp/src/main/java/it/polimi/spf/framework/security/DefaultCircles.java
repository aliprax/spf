/**
 *
 */
package it.polimi.spf.framework.security;

import java.util.Comparator;

/**
 * @author Jacopo
 */
public final class DefaultCircles {

    public static final String PRIVATE = "private";
    public static final String PUBLIC = "public";
    public static final String ALL_CIRCLE = "all_circles";

    public static Comparator<String> COMPARATOR = new Comparator<String>() {

        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(String lhs, String rhs) {
            if (lhs.equals(rhs)) {
                return 0;
            }

            if (lhs.equals(PUBLIC)) {
                return -1;
            }

            if (rhs.equals(PUBLIC)) {
                return 1;
            }

            if (lhs.equals(ALL_CIRCLE)) {
                return -1;
            }

            if (rhs.equals(ALL_CIRCLE)) {
                return 1;
            }

            return lhs.compareTo(rhs);
        }
    };

    private static final String[] all = {PUBLIC, ALL_CIRCLE};

    public static String[] getAll() {
        return all;
    }

    public static boolean isDefault(String circle) {
        if (PRIVATE.equals(circle)) {
            return true;
        }

        for (String def : all) {
            if (def.equals(circle)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     */
    private DefaultCircles() {
        // TODO Auto-generated constructor stub
    }

}
