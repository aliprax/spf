package it.polimi.spf.framework.proximity;

import android.content.Context;
import android.util.Log;

import it.polimi.spf.alljoyn.AlljoynProximityMiddleware;
import it.polimi.spf.wifidirect.WFDMiddlewareAdapter;

/**
 * Interface for components that provides proximity capabilities to SPF.
 *
 * @author darioarchetti
 */
public interface ProximityMiddleware {

    /**
     * Factory class to create instances of {@link ProximityMiddleware}
     *
     * @author darioarchetti
     */
    public static class Factory {

        public static final boolean USE_WFD = true;

        public static ProximityMiddleware newInstance(Context context, InboundProximityInterface iface, String identifier) {
            if (System.getProperty("os.arch").equals("i686")) {
                Log.w("SPF", "Alljoyn middleware not loaded");
                return new DummyMiddleware();
            } else if (USE_WFD) {
                return new WFDMiddlewareAdapter(context, iface, identifier);
            } else {
                return new AlljoynProximityMiddleware(context, iface, identifier);
            }
        }
    }

    public void connect();

    public void disconnect();

    public boolean isConnected();

    public void sendSearchResult(String queryId, String uniqueIdentifier, String baseInfo);

    public void sendSearchSignal(String sender, String searchId, String query);

    public void registerAdvertisement(String advertisedProfile, long sendPeriod);

    public void unregisterAdvertisement();

    public boolean isAdvertising();

}
