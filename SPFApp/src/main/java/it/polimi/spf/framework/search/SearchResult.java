/**
 *
 */
package it.polimi.spf.framework.search;


import it.polimi.spf.shared.model.BaseInfo;

/**
 *
 *
 */
public class SearchResult {

    private String mQueryId;
    private String mUniqueIdentifier;
    private BaseInfo mBaseInfo;

    public SearchResult(String searchId, String uniqueIdentifier, BaseInfo baseInfo) {
        this.mQueryId = searchId;
        this.mUniqueIdentifier = uniqueIdentifier;
        this.mBaseInfo = baseInfo;
    }

    public String getQueryId() {
        return mQueryId;
    }

    public String getUniqueIdentifier() {
        return mUniqueIdentifier;
    }

    public BaseInfo getBaseInfo() {
        return mBaseInfo;
    }

}
