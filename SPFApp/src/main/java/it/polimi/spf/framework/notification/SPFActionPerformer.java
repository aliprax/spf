/**
 *
 */
package it.polimi.spf.framework.notification;


import it.polimi.spf.shared.model.SPFTrigger;

/**
 *
 */
public interface SPFActionPerformer {

    public void perform(SPFAdvProfile target, SPFTrigger trigger);

}
