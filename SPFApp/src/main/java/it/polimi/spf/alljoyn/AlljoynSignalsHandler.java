package it.polimi.spf.alljoyn;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.annotation.BusInterface;
import org.alljoyn.bus.annotation.BusSignal;

@BusInterface(name = "it.polimi.spf.alljoyn.SPFAlljoynSignalsInterface")
public interface AlljoynSignalsHandler {

    @BusSignal
    void searchSignal(String sender, String queryId, String query) throws BusException;

    @BusSignal
    void searchResult(String queryId, String uniqueIdentifier, String baseInfo) throws BusException;

    @BusSignal
    void advertisingSignal(String profile, String uniqueIdentifier) throws BusException;
}
