/**
 *
 */
package it.polimi.spf.alljoyn;

import org.alljoyn.bus.annotation.BusInterface;
import org.alljoyn.bus.annotation.BusMethod;

import it.polimi.spf.framework.proximity.InboundProximityInterface;

/**
 * Inbound proximity interface exposed by {@link AlljoynProximityMiddleware} on the Alljoyn bus to
 * allow the reception of requests from remote interfaces. The actual implementation, {@link
 * AlljoynProximityInterfaceImpl} acts as an adapter that forwards the request to a instance of
 * {@link InboundProximityInterface} created by SPF.
 */

@BusInterface(name = "it.polimi.spf.alljoyn.proximityinterface")
public interface AlljoynProximityInterface {

    /**
     * Dispatch an invocation request for a service registered on SPF
     *
     * @param marshalledRequest - the json representation of an {@link it.polimi.spf.shared.model.InvocationRequest}
     *
     * @return - the json representation of an {@link it.polimi.spf.shared.model.InvocationResponse}
     */
    @BusMethod
    public String executeService(String marshalledRequest);

    /**
     * Retrieves the values of a collection of {@link it.polimi.spf.shared.model.ProfileField} from
     * the profile
     *
     * @param token
     * @param appIdentifier
     * @param fieldList
     *
     * @return
     */
    @BusMethod
    public String getProfileBulk(String token, String appIdentifier, String fieldList);

    /**
     * @param friendshipMessageJSON
     */
    @BusMethod
    public void sendFriendshipMessage(String friendshipMessageJSON);

    /**
     * @param uniqueIdentifier
     * @param actionSendNotification
     */
    @BusMethod
    public void sendNotification(String uniqueIdentifier, String actionSendNotification);

    /**
     * Sends a marshalled {@link it.polimi.spf.shared.model.SPFActivity} to this instance.
     *
     * @param activity - the marshalled activity
     */
    public String sendActivity(String activity);

}
