package it.polimi.spf.wfdm;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

public class WfdOutputStream {
	
	private PrintWriter writer;
	
	public WfdOutputStream(OutputStream outputStream) {
		this.writer = new PrintWriter(outputStream);
		
	}

	public void writeMessage(WfdMessage msg) throws IOException {
		String str = msg.toString();
		writer.println(str);
		writer.flush();
		if(writer.checkError()){
			throw new IOException();
		}
		
	}

}
