package it.polimi.spf.wfdm;

import android.util.Log;

public class WfdLog {

	public static boolean ENABLED = false;

	static void d(String tag, String msg) {
		if (ENABLED) {
			Log.d(tag, msg);
		}
	}

	static void d(String tag, String msg, Throwable tr) {
		if (ENABLED) {
			Log.d(tag, msg, tr);
		}
	}

	static void e(String tag, String msg, Throwable tr) {
		if (ENABLED) {
			Log.e(tag, msg, tr);
		}
	}

}
