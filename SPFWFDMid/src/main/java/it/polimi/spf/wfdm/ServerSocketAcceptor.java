package it.polimi.spf.wfdm;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

class ServerSocketAcceptor extends Thread {
	public static final String TAG  = "ServerSocketAcceptor";
	private GroupOwnerActor groupOwner;
	private ServerSocket serverSocket;
	boolean closed;

	public ServerSocketAcceptor(GroupOwnerActor groupOwner,
			ServerSocket serverSocket) {
		this.groupOwner = groupOwner;
		this.serverSocket = serverSocket;
	}

	public void recycle() {
		this.closed = true;
		interrupt();
	}

	@Override
	public void run() {
		Socket s;
		try {
			while (!Thread.currentThread().isInterrupted()) {
				WfdLog.d(TAG, "accept(): waiting for a new client");
				s = serverSocket.accept();
				WfdLog.d(TAG, "incoming connection");
				new GOInternalClient(s,groupOwner).start();
			}
		} catch (IOException e) {

		}
		WfdLog.d(TAG, "exiting while loop");
		if (!closed) {
			WfdLog.d(TAG, "signalling error to groupOwnerActor");
			groupOwner.onServerSocketError();
		}
	}

}
