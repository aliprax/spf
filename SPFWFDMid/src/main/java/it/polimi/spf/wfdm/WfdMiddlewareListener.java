package it.polimi.spf.wfdm;

public interface WfdMiddlewareListener {
	
	void onMessageReceived(WfdMessage msg);
	void onInstanceFound(String identifier);
	void onInstanceLost(String identifier);
	void onError();
	WfdMessage onRequestMessageReceived(WfdMessage msg);
}
