package it.polimi.spf.wfdm;

class ResponseHolder {

	private final long REQ_TIMEOUT;
	private WfdMessage msg;
	private boolean threadWaiting = false;
	private long requestSequenceId = 0;

	public ResponseHolder(long timeout) {
		this.REQ_TIMEOUT = timeout;
	}
	
	public synchronized void set(WfdMessage msg) {
		long sequenceNum = msg.getTimestamp();

		if (threadWaiting && sequenceNum == requestSequenceId) {
			this.msg = msg;
			notify();
		}
	}

	public synchronized WfdMessage get() throws InterruptedException {
		if (msg == null) {
			threadWaiting = true;
			wait(REQ_TIMEOUT);
			WfdMessage _tmpMsg = msg;
			msg = null;
			threadWaiting = false;
			return _tmpMsg;
		}
		return null;
	}

	public long assignRequestSequenceId() {
		return ++requestSequenceId;
	}
}