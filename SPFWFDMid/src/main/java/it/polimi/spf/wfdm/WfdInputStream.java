package it.polimi.spf.wfdm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

class WfdInputStream {
	BufferedReader reader;

	public WfdInputStream(InputStream inputStream) {
		this.reader = new BufferedReader(new InputStreamReader(inputStream));
	}

	public WfdMessage readMessage() throws IOException {
		String str = reader.readLine();
		return WfdMessage.fromString(str);
	}

	public WfdMessage readMessage(long l) throws InterruptedException {
		TimedRead tr = new TimedRead();
		tr.start();
		String str = tr.readResult(l);
		if (str != null) {
			return WfdMessage.fromString(str);
		}
		return null;
	}

	class TimedRead extends Thread {
		
		String str = null;

		@Override
		public void run() {
			try {
				str = WfdInputStream.this.reader.readLine();

			} catch (IOException e) {

			} finally {
				synchronized (this) {
					notify();
				}
			}
		}

		String readResult(long millis) throws InterruptedException {
			synchronized (this) {
				if (str == null) {
					wait(millis);
				}
				return str;
			}
		}
	}

}
