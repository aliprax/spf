package it.polimi.spf.wfdm;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import android.os.Handler;
import android.os.Looper;

class GroupClientActor extends GroupActor {

	private static final String TAG = "GroupClientActor";
	private InetAddress groupOwnerAddress;
	private int destPort;
	private Socket socket;
	private boolean closed = false;

	GroupClientActor(InetAddress groupOwnerAddress, int destPort,
			GroupActorListener listener, String myIdentifier) {
		super(listener,myIdentifier);
		this.groupOwnerAddress = groupOwnerAddress;
		this.destPort = destPort;
	}

	void connect() {

		t.start();
	}

	void disconnect() {
		try {
			WfdLog.d(TAG, "Disconnect called");
			t.interrupt();
			socket.close();
		} catch (IOException e) {
			WfdLog.d(TAG, "error on closing socket", e);
		}
	}
	
	Thread t = new Thread() {
		@Override
		public void run() {
			WfdInputStream inStream;
			try {
				WfdLog.d(TAG, "Opening socket connection");
				socket = new Socket();
				SocketAddress remoteAddr = new InetSocketAddress(
						groupOwnerAddress, destPort);
				socket.connect(remoteAddr, 1000);
				inStream = new WfdInputStream(socket.getInputStream());
				establishConnection();
				WfdLog.d(TAG, "Entering read loop");
				while (!isInterrupted()) {
					WfdMessage msg = inStream.readMessage();
					WfdLog.d(TAG, "message received");
					GroupClientActor.super.handle(msg);
				}
			} catch (Throwable e) {
				WfdLog.d(TAG, "error in the run loop", e);
			} finally {
				closeSocket();
			}
			if (!closed) {
				new Handler(Looper.getMainLooper()).post(new Runnable() {

					@Override
					public void run() {
						GroupClientActor.super.onError();
					}
				});
			}
		}
	};

	private void closeSocket() {
		try {
			socket.close();
		} catch (IOException e) {
		}
	}

	private void establishConnection() throws IOException {
		WfdMessage msg = new WfdMessage();
		msg.setType(WfdMessage.TYPE_CONNECT);
		msg.setSenderId(getIdentifier());
		WfdLog.d(TAG, "Sending connection message... ");
		sendMessage(msg);
	}

	@Override
	void sendMessage(WfdMessage msg) throws IOException {
		WfdLog.d(TAG, "Sending message");
		WfdOutputStream outstream = new WfdOutputStream(
				socket.getOutputStream());
		outstream.writeMessage(msg);
	}



}
