/**
 *
 */
package it.polimi.spf.shared.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * This class defines the standards errors signaled by the framework.
 */
public class SPFError implements Parcelable {

    public static final int NONE_ERROR_CODE = 0;
    public static final int TOKEN_NOT_VALID_ERROR_CODE = 1;
    public static final int PERMISSION_DENIED_ERROR_CODE = 2;
    public static final int REMOTE_EXC_ERROR_CODE = 3;
    public static final int INSTANCE_NOT_FOUND_ERROR_CODE = 4;
    public static final int NETWORK_ERROR_CODE = 5;
    public static final int ILLEGAL_ARGUMENT_ERROR_CODE = 6;
    public static final int REGISTRATION_REFUSED_ERROR_CODE = 7;
    public static final int INTERNAL_SPF_ERROR_CODE = 8;
    public static final int SPF_NOT_INSTALLED_ERROR_CODE = 9;

    private int code;
    private String message;

    public SPFError(int errorCode) {
        this.code = errorCode;
    }

    public SPFError() {
        this.code = NONE_ERROR_CODE;
    }

    ;

    public boolean codeEquals(int errorCode) {
        return this.code == errorCode;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + code;
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SPFError)) {
            return false;
        }
        SPFError other = (SPFError) obj;
        if (code != other.code) {
            return false;
        }
        return true;
    }

    ;

    SPFError(Parcel source) {
        this.code = source.readInt();
    }

    public static Creator<SPFError> CREATOR = new Creator<SPFError>() {

        @Override
        public SPFError[] newArray(int size) {

            return new SPFError[size];
        }

        @Override
        public SPFError createFromParcel(Parcel source) {
            return new SPFError(source);
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
        dest.writeString(message);
    }

    public void readFromParcel(Parcel source) {
        this.code = source.readInt();
        this.message = source.readString();
    }

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }

    public void setError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public boolean isOk() {
        return code == NONE_ERROR_CODE;
    }

    @Override
    public String toString() {
        String string;
        switch (code) {
            case NONE_ERROR_CODE:
                string = "NONE_ERROR";
                break;
            case TOKEN_NOT_VALID_ERROR_CODE:
                string = "TOKEN_NOT_VALID";
                break;
            case PERMISSION_DENIED_ERROR_CODE:
                string = "PERMISSION_DENIED";
                break;
            case REMOTE_EXC_ERROR_CODE:
                string = "REMOTE_EXCEPTION";
                break;
            case INSTANCE_NOT_FOUND_ERROR_CODE:
                string = "INSTANCE_NOT_FOUND";
                break;
            case NETWORK_ERROR_CODE:
                string = "NETWORK_ERROR";
                break;
            case ILLEGAL_ARGUMENT_ERROR_CODE:
                string = "ILLEGAL_ARUMENT";
                break;
            case REGISTRATION_REFUSED_ERROR_CODE:
                string = "REGISTRATION_REFUSED";
                break;
            case INTERNAL_SPF_ERROR_CODE:
                string = "INTERNAL_SPF_ERROR";
                break;
            default:
                string = "UNKNOWN_ERROR";
        }
        return string;
    }

}
