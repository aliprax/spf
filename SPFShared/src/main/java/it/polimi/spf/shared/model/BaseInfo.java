package it.polimi.spf.shared.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Container for basic information about a person that is transmitted together with search results.
 *
 * BaseInfo carries the following details: <ul> <li>Identifier</li> <li>Display name</li> </ul>
 *
 * @author darioarchetti
 */
public class BaseInfo implements Parcelable {

    private static final String DISPLAY_NAME = "displayName";
    private static final String IDENTIFIER = "identifier";
    private static final String TAG = "BaseInfo";

    private final String mIdentifier, mDisplayName;

    /**
     * Unflattens a {@link BaseInfo} instance from a JSON representation created with {@link
     * BaseInfo#toJSONString()}
     *
     * @param json - the JSON string representing the original instance
     *
     * @return the unflettened instance
     */
    public static BaseInfo fromJSONString(String json) {
        try {
            JSONObject o = new JSONObject(json);
            return new BaseInfo(o.getString(IDENTIFIER), o.getString(DISPLAY_NAME));
        } catch (JSONException e) {
            Log.d(TAG, "Error unmarshalling baseinfo: ", e);
            return null;
        }
    }

    /**
     * Creates a new instance of {@link BaseInfo} with the provided details.
     *
     * @param mIdentifier  - the identifier (see {@link ProfileField#IDENTIFIER})
     * @param mDisplayName - the display name (see {@link ProfileField#DISPLAY_NAME})
     */
    public BaseInfo(String mIdentifier, String mDisplayName) {
        this.mIdentifier = mIdentifier;
        this.mDisplayName = mDisplayName;
    }

    public BaseInfo(Parcel source) {
        mIdentifier = source.readString();
        mDisplayName = source.readString();
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return mIdentifier;
    }

    /**
     * @return the display name
     */
    public String getDisplayName() {
        return mDisplayName;
    }

    /**
     * Flattens the instance into a JSON String that can be later unflattened with {@link
     * BaseInfo#fromJSONString(String)}
     *
     * @return - the flattened representation
     */
    public String toJSONString() {
        JSONObject object = new JSONObject();

        try {
            object.put(IDENTIFIER, mIdentifier);
            object.put(DISPLAY_NAME, mDisplayName);
        } catch (JSONException e) {
            Log.d(TAG, "Error marshalling baseinfo", e);
        }

        return object.toString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mIdentifier);
        dest.writeString(mDisplayName);
    }

    public static final Creator<BaseInfo> CREATOR = new Creator<BaseInfo>() {

        @Override
        public BaseInfo[] newArray(int size) {
            return new BaseInfo[size];
        }

        @Override
        public BaseInfo createFromParcel(Parcel source) {
            return new BaseInfo(source);
        }
    };
}
