package it.polimi.spf.shared.model;

public enum Permission {

    REGISTER_SERVICES(0x1),
    SEARCH_SERVICE(0x2),
    EXECUTE_LOCAL_SERVICES(0x4),
    EXECUTE_REMOTE_SERVICES(0x8),
    READ_LOCAL_PROFILE(0x10),
    READ_REMOTE_PROFILES(0x20),
    WRITE_LOCAL_PROFILE(0x40),
    NOTIFICATION_SERVICES(0x80),
    ACTIVITY_SERVICE(0x100);

    private int mCode;

    private Permission(int code) {
        this.mCode = code;
    }

    public int getCode() {
        return mCode;
    }

}
