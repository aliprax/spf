package it.polimi.spf.shared.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * TODO SECURITY REFACTOR - reuse in app registration remove intent add: name permission (later) app
 * icon (feasible?)
 *
 * @author darioarchetti
 */
public class AppDescriptor implements Parcelable {

    private String mAppIdentifier;
    private String mAppName;
    private String mVersion;
    private int mPermissionCodes;

    public AppDescriptor(String appIdentifier, String appName, String version, int permissionCodes) {
        this.mAppIdentifier = appIdentifier;
        this.mAppName = appName;
        this.mVersion = version;
        this.mPermissionCodes = permissionCodes;
    }

    private AppDescriptor(Parcel source) {
        this.mAppIdentifier = source.readString();
        this.mAppName = source.readString();
        this.mVersion = source.readString();
        this.mPermissionCodes = source.readInt();
    }

    public String getAppIdentifier() {
        return mAppIdentifier;
    }

    public String getAppName() {
        return mAppName;
    }

    public String getVersion() {
        return mVersion;
    }

    public int getPermissionCode() {
        return mPermissionCodes;
    }

    @Override
    public String toString() {
        return mAppIdentifier + "-" + mVersion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mAppIdentifier);
        dest.writeString(mAppName);
        dest.writeString(mVersion);
        dest.writeInt(mPermissionCodes);
    }

    public final static Creator<AppDescriptor> CREATOR = new Creator<AppDescriptor>() {

        @Override
        public AppDescriptor[] newArray(int size) {
            return new AppDescriptor[size];
        }

        @Override
        public AppDescriptor createFromParcel(Parcel source) {
            return new AppDescriptor(source);
        }
    };

}
