package it.polimi.spf.shared.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Array;
import java.util.Date;

public abstract class ProfileFieldConverter<E extends Object> {

    private static final ProfileFieldConverter<String> STRING_CONVERTER = new StringConverter();
    private static final ProfileFieldConverter<Date> DATE_CONVERTER = new DateConverter();
    private static final ProfileFieldConverter<String[]> STRING_ARRAY_CONVERTER = new ArrayConverter<String>(String.class, STRING_CONVERTER);
    private static final ProfileFieldConverter<Bitmap> BITMAP_CONVERTER = new BitmapConverter();
    private static final String ARRAY_SEPARATOR = ";";

    @SuppressWarnings("unchecked")
    public static <E> ProfileFieldConverter<E> forField(ProfileField<E> field) {
        Class<?> fieldClass = field.getFieldClass();

        if (fieldClass == String.class) {
            return (ProfileFieldConverter<E>) STRING_CONVERTER;
        } else if (fieldClass == Date.class) {
            return (ProfileFieldConverter<E>) DATE_CONVERTER;
        } else if (fieldClass == String[].class) {
            return (ProfileFieldConverter<E>) STRING_ARRAY_CONVERTER;
        } else if (fieldClass == Bitmap.class) {
            return (ProfileFieldConverter<E>) BITMAP_CONVERTER;
        }

        throw new IllegalArgumentException("Field type " + fieldClass.getSimpleName() + " not supported");
    }

    // public methods
    public abstract String toStorageString(E value);

    public abstract E fromStorageString(String storageString);

    // Implementations
    private static class StringConverter extends ProfileFieldConverter<String> {

        @Override
        public String toStorageString(String value) {
            return value;
        }

        @Override
        public String fromStorageString(String storageString) {
            return storageString;
        }

    }

    private static class DateConverter extends ProfileFieldConverter<Date> {

        @Override
        public String toStorageString(Date value) {
            return String.valueOf(value.getTime());
        }

        @Override
        public Date fromStorageString(String storageString) {
            return new Date(Long.valueOf(storageString));
        }
    }

    private static class ArrayConverter<E> extends ProfileFieldConverter<E[]> {

        private Class<E> mComponentType;
        private ProfileFieldConverter<E> mBaseConverter;

        public ArrayConverter(Class<E> componentType, ProfileFieldConverter<E> baseConverter) {
            this.mComponentType = componentType;
            this.mBaseConverter = baseConverter;
        }

        @Override
        public String toStorageString(E[] value) {
            StringBuilder b = new StringBuilder();
            for (int i = 0; i < value.length; i++) {
                b.append(mBaseConverter.toStorageString(value[i]));
                if ((i + 1) < value.length) {
                    b.append(ARRAY_SEPARATOR);
                }
            }
            return b.toString();
        }

        @SuppressWarnings("unchecked")
        @Override
        public E[] fromStorageString(String storageString) {
            String[] values = storageString.split(ARRAY_SEPARATOR);
            Object array = Array.newInstance(mComponentType, values.length);

            for (int i = 0; i < values.length; i++) {
                Array.set(array, i, mBaseConverter.fromStorageString(values[i]));
            }

            return (E[]) array;
        }

    }

    private static class BitmapConverter extends ProfileFieldConverter<Bitmap> {

        @Override
        public String toStorageString(Bitmap value) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            value.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
        }

        @Override
        public Bitmap fromStorageString(String storageString) {
            byte[] ba = Base64.decode(storageString.getBytes(), Base64.DEFAULT);
            Bitmap photo = BitmapFactory.decodeByteArray(ba, 0, ba.length);
            return photo;
        }
    }
}
