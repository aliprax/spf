package it.polimi.spf.shared.model;

import android.os.Parcel;
import android.os.Parcelable;

public class InvocationResponse implements Parcelable {

    public final static int RESULT = 0;
    public final static int ERROR = 1;

    private final int type;
    private String errorMessage;
    private Object result;

    public static InvocationResponse result(Object result) {
        InvocationResponse resp = new InvocationResponse(RESULT);
        resp.result = result;
        return resp;
    }

    public static InvocationResponse error(Throwable t) {
        InvocationResponse resp = new InvocationResponse(ERROR);
        resp.errorMessage = t.getClass().getName() + ":" + t.getLocalizedMessage();
        return resp;
    }

    public static InvocationResponse error(String errorMessage) {
        InvocationResponse resp = new InvocationResponse(ERROR);
        resp.errorMessage = errorMessage;
        return resp;
    }

    private InvocationResponse(int type) {
        this.type = type;
    }

    private InvocationResponse(Parcel source) {
        type = source.readInt();
        switch (type) {
            case ERROR:
                errorMessage = source.readString();
                break;
            case RESULT:
                result = source.readValue(null);
                break;
        }
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public int getType() {
        return type;
    }

    public boolean isResult() {
        return type == RESULT;
    }

    @Override
    public String toString() {
        return "Invocation response: " + (type == RESULT ? "RESULT\n" + result : "ERROR\n" + errorMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flag) {
        dest.writeInt(type);
        switch (type) {
            case ERROR:
                dest.writeString(errorMessage);
                break;
            case RESULT:
                dest.writeValue(result);
                break;
        }
    }

    public final static Creator<InvocationResponse> CREATOR = new Creator<InvocationResponse>() {

        @Override
        public InvocationResponse[] newArray(int arg0) {
            return new InvocationResponse[arg0];
        }

        @Override
        public InvocationResponse createFromParcel(Parcel arg0) {
            return new InvocationResponse(arg0);
        }
    };
}
