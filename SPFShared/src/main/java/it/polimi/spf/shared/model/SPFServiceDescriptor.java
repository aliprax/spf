package it.polimi.spf.shared.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SPFServiceDescriptor implements Parcelable {

    private String mServiceName;
    private String mAppIdentifier;
    private String mVersion;
    private String mIntent;
    private String[] mConsumedVerbs;

    public SPFServiceDescriptor(String svcName, String appIdentifier, String version, String intent, String[] consumedVerbs) {
        this.mServiceName = svcName;
        this.mAppIdentifier = appIdentifier;
        this.mVersion = version;
        this.mIntent = intent;
        this.mConsumedVerbs = consumedVerbs;
    }

    private SPFServiceDescriptor(Parcel source) {
        this.mServiceName = source.readString();
        this.mAppIdentifier = source.readString();
        this.mVersion = source.readString();
        this.mIntent = source.readString();
        this.mConsumedVerbs = source.createStringArray();
    }

    public String getServiceName() {
        return mServiceName;
    }

    public String getAppIdentifier() {
        return mAppIdentifier;
    }

    public String getVersion() {
        return mVersion;
    }

    public String getIntent() {
        return mIntent;
    }

    public String[] getConsumedVerbs() {
        return mConsumedVerbs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mServiceName);
        dest.writeString(mAppIdentifier);
        dest.writeString(mVersion);
        dest.writeString(mIntent);
        dest.writeStringArray(mConsumedVerbs);
    }

    public final static Creator<SPFServiceDescriptor> CREATOR = new Creator<SPFServiceDescriptor>() {

        @Override
        public SPFServiceDescriptor createFromParcel(Parcel source) {
            return new SPFServiceDescriptor(source);
        }

        @Override
        public SPFServiceDescriptor[] newArray(int size) {
            return new SPFServiceDescriptor[size];
        }

    };
}