package it.polimi.spf.shared.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

public class InvocationRequest implements Parcelable {

    private String appName;
    private String serviceName;
    private String methodName;
    private Object[] params;

    public InvocationRequest(String appName, String serviceName, String methodName, Object[] params) {
        this.appName = appName;
        this.serviceName = serviceName;
        this.methodName = methodName;
        this.params = params;
    }

    private InvocationRequest(Parcel source) {
        appName = source.readString();
        serviceName = source.readString();
        methodName = source.readString();
        params = source.readArray(null);
    }

    public String getAppName() {
        return appName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getMethodName() {
        return methodName;
    }

    public Object[] getParams() {
        return params;
    }

    @Override
    public String toString() {
        return appName + " - " + serviceName + "." + methodName + Arrays.toString(params);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(appName);
        dest.writeString(serviceName);
        dest.writeString(methodName);
        dest.writeArray(params);
    }

    public static Creator<InvocationRequest> CREATOR = new Creator<InvocationRequest>() {

        @Override
        public InvocationRequest[] newArray(int size) {
            return new InvocationRequest[size];
        }

        @Override
        public InvocationRequest createFromParcel(Parcel source) {
            return new InvocationRequest(source);
        }
    };

}
