package it.polimi.spf.shared.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

/**
 * @author darioarchetti
 */
public class SPFActivity implements Parcelable {

    public static final String VERB = "verb";
    public static final String SENDER_IDENTIFIER = "senderIdentifier";
    public static final String SENDER_DISPLAY_NAME = "senderDisplayName";
    public static final String RECEIVER_IDENTIFIER = "receiverIdentifier";
    public static final String RECEIVER_DISPLAY_NAME = "receiverDisplayName";

    private final Map<String, String> mFields;

    public void put(String key, String value) {
        if (key.equals(VERB)) {
            return;
        }

        mFields.put(key, value);
    }

    public String get(String key) {
        return mFields.get(key);
    }

    public SPFActivity(String verb) {
        this();
        if (verb == null) {
            throw new NullPointerException();
        }

        mFields.put(VERB, verb);
    }

    private SPFActivity() {
        this.mFields = new HashMap<String, String>();
    }

    public String getVerb() {
        return mFields.get(VERB);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Bundle b = new Bundle();

        for (String k : mFields.keySet()) {
            b.putString(k, mFields.get(k));
        }

        b.writeToParcel(dest, 0);
    }

    public static final Creator<SPFActivity> CREATOR = new Creator<SPFActivity>() {

        @Override
        public SPFActivity[] newArray(int size) {
            return new SPFActivity[size];
        }

        @Override
        public SPFActivity createFromParcel(Parcel source) {
            SPFActivity ac = new SPFActivity();
            ac.readFromParcel(source);
            return ac;
        }
    };

    public void readFromParcel(Parcel parcel) {
        Bundle b = parcel.readBundle();
        for (String k : b.keySet()) {
            mFields.put(k, b.getString(k));
        }
    }
}
