package it.polimi.spf.shared.aidl;

import it.polimi.spf.shared.model.SPFTrigger;
import it.polimi.spf.shared.model.SPFError;
import java.util.List;

interface SPFNotificationService{

  long saveTrigger(in SPFTrigger trigger,in String accessToken, out SPFError err);
  boolean deleteTrigger(in long triggerId, in String token, out SPFError err);
  boolean deleteAllTrigger( in String token, out SPFError err );
  List<SPFTrigger> listTrigger(in String token, out SPFError err);
  SPFTrigger getTrigger(in long triggerId,String token, out SPFError err);

}