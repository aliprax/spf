package it.polimi.spf.shared.aidl;

import it.polimi.spf.shared.model.ProfileFieldContainer;
import it.polimi.spf.shared.model.SPFError;

/**
 * Service to manage local profile.
 */
interface LocalProfileService {

  ProfileFieldContainer getValueBulk(String accessToken, in String[] profileFieldIdentifiers, out SPFError err);

  void setValueBulk(String accessToken, in ProfileFieldContainer container, out SPFError err);

}