package it.polimi.spf.shared.aidl;

import it.polimi.spf.shared.model.AppDescriptor;
import it.polimi.spf.shared.model.SPFServiceDescriptor;
import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.SPFError;
import it.polimi.spf.shared.model.SPFActivity;

/**
 * Service that allows applications to register services
 * to be made available for execution, and to execute the
 * services of local applications.
 */
interface LocalServiceManager {
	InvocationResponse sendActivityLocally(String accessToken, in SPFActivity activity, out SPFError error);
    InvocationResponse executeLocalService(String accessToken, in InvocationRequest request, out SPFError error);
    boolean registerService(String accessToken, in SPFServiceDescriptor descriptor, out SPFError error);
    boolean unregisterService(String accessToken, in SPFServiceDescriptor descriptor, out SPFError error);
    void injectInformationIntoActivity(String accessToken, inout SPFActivity activity, out SPFError error);
}