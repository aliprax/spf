package it.polimi.spf.shared.aidl;

import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.SPFActivity;

interface ClientExecutionService {

	InvocationResponse executeService(in InvocationRequest request);
	InvocationResponse sendActivity(in SPFActivity activity);

}