package it.polimi.spf.shared.aidl;

import it.polimi.spf.shared.model.AppDescriptor;
import it.polimi.spf.shared.aidl.SPFAppRegistrationCallback;

/*
 * Service that allows applications to register themselves
 * and obtain an access token to perform requests to other
 * services of the framework.
 */
interface SPFSecurityService {
	void registerApp(in AppDescriptor descriptor, SPFAppRegistrationCallback callback);
	void unregisterApp(String accessToken);
}