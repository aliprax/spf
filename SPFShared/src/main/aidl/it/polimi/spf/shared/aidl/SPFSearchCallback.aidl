package it.polimi.spf.shared.aidl;

import it.polimi.spf.shared.model.BaseInfo;

oneway interface SPFSearchCallback{
    
    void onSearchResultReceived(String queryId,String uniqueIdentifier, in BaseInfo baseInfo);
    void onSearchResultLost(String queryId,String uniqueIdentifier);
    void onSearchStart(String queryId);
    void onSearchStop(String queryId);
    void onSearchError(String queryId);
}