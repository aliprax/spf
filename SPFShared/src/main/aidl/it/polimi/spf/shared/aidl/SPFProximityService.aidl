package it.polimi.spf.shared.aidl;

import it.polimi.spf.shared.model.InvocationResponse;
import it.polimi.spf.shared.model.InvocationRequest;
import it.polimi.spf.shared.model.SPFServiceDescriptor;
import it.polimi.spf.shared.model.AppDescriptor;
import it.polimi.spf.shared.model.SPFSearchDescriptor;
import it.polimi.spf.shared.model.ProfileFieldContainer;
import it.polimi.spf.shared.model.SPFError;
import it.polimi.spf.shared.model.SPFActivity;
import it.polimi.spf.shared.aidl.SPFSearchCallback;

/*
 * Services that allows the interaction with remote
 * instances of SPF
 */
interface SPFProximityService {
  
    // Method execution
    InvocationResponse executeRemoteService(String accessToken, String target, in InvocationRequest request, out SPFError err);
    InvocationResponse sendActivity(String accessToken, String target, in SPFActivity activity, out SPFError err);
    void injectInformationIntoActivity(String accessToken, String target, inout SPFActivity activity, out SPFError error);
    
    // Profile information retrieval
    ProfileFieldContainer getProfileBulk(String accessToken, String target, in String[] fieldIdentifiers, out SPFError err);
    
    // Search for people
    void registerCallback(in String accessToken, SPFSearchCallback callback, out SPFError err);
    boolean unregisterCallback(in String accessToken, out SPFError err);
    String startNewSearch(in String accessToken, in SPFSearchDescriptor searchDescriptor, out SPFError err);
    void stopSearch(in String accessToken, in String queryId, out SPFError err);
    boolean lookup(in String accessToken, String personIdentifier, out SPFError err);
}