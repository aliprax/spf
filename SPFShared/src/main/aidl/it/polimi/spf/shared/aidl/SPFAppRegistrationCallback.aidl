package it.polimi.spf.shared.aidl;

interface SPFAppRegistrationCallback {
	void onRegistrationSuccess(String accessToken);
	void onRegistrationFailure();
}